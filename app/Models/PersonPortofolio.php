<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PersonPortofolio
 * @package App\Models
 * @version December 9, 2021, 8:34 am UTC
 *
 * @property \App\Models\Person $person
 * @property string $name
 * @property string $year
 * @property string $company
 * @property string $link
 * @property integer $person_id
 */
class PersonPortofolio extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'person_potofolio';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'year',
        'company',
        'link',
        'person_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'year' => 'string',
        'company' => 'string',
        'link' => 'string',
        'person_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'name' => 'nullable|string',
//        'year' => 'nullable|string',
//        'company' => 'nullable|string',
//        'link' => 'nullable|string',
//        'created_at' => 'nullable',
//        'updated_at' => 'nullable',
//        'deleted_at' => 'nullable',
//        'person_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function person()
    {
        return $this->belongsTo(\App\Models\Person::class, 'person_id');
    }
}
