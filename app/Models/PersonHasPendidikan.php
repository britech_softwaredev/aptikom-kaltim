<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PersonHasPendidikan
 * @package App\Models
 * @version June 11, 2021, 1:33 am UTC
 *
 * @property \App\Models\Pendidikan $pendidikan
 * @property \App\Models\Person $person
 * @property integer $person_id
 * @property integer $pendidikan_id
 * @property string $perguruan_tinggi
 * @property string $program_studi
 * @property string $tahun_lulus
 * @property string $person_has_pendidikancol
 * @method static create(array $array)
 */
class PersonHasPendidikan extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'person_has_pendidikan';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'person_id',
        'pendidikan_id',
        'tahun_lulus',
        'perguruan_tinggi',
        'program_studi',
        'person_has_pendidikancol'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'person_id' => 'integer',
        'pendidikan_id' => 'integer',
        'perguruan_tinggi' => 'string',
        'program_studi' => 'string',
        'tahun_lulus' => 'string',
        'person_has_pendidikancol' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'person_id' => 'required',
//        'pendidikan_id' => 'required',
//        'perguruan_tinggi' => 'nullable|string|max:255',
//        'program_studi' => 'nullable|string|max:255',
//        'tahun_lulus' => 'nullable|string|max:45',
//        'person_has_pendidikancol' => 'nullable|string|max:45',
//        'created_at' => 'nullable',
//        'updated_at' => 'nullable',
//        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pendidikan()
    {
        return $this->belongsTo(\App\Models\Pendidikan::class, 'pendidikan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function person()
    {
        return $this->belongsTo(\App\Models\Person::class, 'person_id');
    }
}
