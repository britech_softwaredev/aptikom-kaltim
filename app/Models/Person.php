<?php

namespace App\Models;

use CyrildeWit\EloquentViewable\InteractsWithViews;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Person
 * @package App\Models
 * @version June 11, 2021, 1:32 am UTC
 *
 * @property \App\Models\Jabatan $jabatan
 * @property \Illuminate\Database\Eloquent\Collection $certificates
 * @property \Illuminate\Database\Eloquent\Collection $personHasPendidikans
 * @property string $name
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property string $address
 * @property string $slug
 * @property string $foto
 * @property integer $order
 * @property integer $jabatan_id
 * @method static create(array $array)
 */
class Person extends Model implements HasMedia
{
    use SoftDeletes;
    use HasFactory;
    use HasSlug;
    use InteractsWithMedia;
    use InteractsWithViews;

    public $table = 'person';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this
            ->addMediaConversion('preview')
            ->fit(Manipulations::FIT_CROP, 300, 300)
            ->nonQueued();

        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10);

        $this->addMediaConversion('cover')
            ->width(2400)
            ->height(1800);
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')->doNotGenerateSlugsOnUpdate();
    }

    public function scopeFindBySlug($query, $slug)
    {
        return $query->where('slug',$slug)->get();
    }
    

    public $fillable = [
        'name_full',
        'date_of_birth',
        'place_of_birth',
        'address',
        'slug',
        'foto',
        'order',
        'jabatan_id',
        'description',
        'join_company',
        'no_anggota',
        'persone',
        'via',
        'masa_berlaku',
        'email_valid',
        'no_hp',
        'users_id',
        'perguruan_tinggi',
        'alamat_perguruan_tinggi',
        'jabatan_perguruan_tinggi',
        'program_studi',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name_full' => 'string',
        'date_of_birth' => 'date',
        'place_of_birth' => 'string',
        'address' => 'string',
        'slug' => 'string',
        'foto' => 'string',
        'order' => 'integer',
        'jabatan_id' => 'integer',
        'description' => 'string',
        'join_company' => 'date',
        'no_anggota' => 'string',
        'program_studi' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'name' => 'required|string|max:255',
//        'date_of_birth' => 'nullable',
//        'place_of_birth' => 'nullable|string|max:255',
//        'created_at' => 'nullable',
//        'updated_at' => 'nullable',
//        'deleted_at' => 'nullable',
//        'address' => 'nullable|string|max:255',
//        'slug' => 'nullable|string|max:255',
//        'foto' => 'nullable|string|max:255',
//        'order' => 'nullable|integer',
//        'jabatan_id' => 'nullable',
//        'alamat_perguruan_tinggi' => 'string',
//        'jabatan' => 'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function jabatan()
    {
        return $this->belongsTo(\App\Models\Jabatan::class, 'jabatan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function certificates()
    {
        return $this->hasMany(\App\Models\Certificate::class, 'person_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function personHasPendidikans()
    {
        return $this->hasMany(\App\Models\PersonHasPendidikan::class, 'person_id');
    }

    public function personSkill()
    {
        return $this->hasMany(\App\Models\PersonSkill::class, 'person_id');
    }

    public function sosmed()
    {
        return $this->hasMany(\App\Models\SosmedPerson::class, 'person_id');
    }

    public function personPorto()
    {
        return $this->hasMany(\App\Models\PersonPortofolio::class, 'person_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'users_id');
    }
}
