<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Pendidikan
 * @package App\Models
 * @version March 16, 2021, 7:53 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $personDewans
 * @property \Illuminate\Database\Eloquent\Collection $personSekretariats
 * @property string $name
 * @property string $definition
 */
class Pendidikan extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'pendidikan';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'definition'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'definition' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'definition' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function personDewans()
    {
        return $this->hasMany(\App\Models\PersonDewan::class, 'pendidikan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function personSekretariats()
    {
        return $this->hasMany(\App\Models\PersonSekretariat::class, 'pendidikan_id');
    }
}
