<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Skill
 * @package App\Models
 * @version December 9, 2021, 8:28 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $personSkills
 * @property string $name
 * @property string $description
 */
class Skill extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'skill';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'nullable|string|max:45',
        'description' => 'nullable|string|max:45',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function personSkills()
    {
        return $this->hasMany(\App\Models\PersonSkill::class, 'skill_id');
    }
}
