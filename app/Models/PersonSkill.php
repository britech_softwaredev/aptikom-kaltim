<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PersonSkill
 * @package App\Models
 * @version December 9, 2021, 8:29 am UTC
 *
 * @property \App\Models\Person $person
 * @property \App\Models\Skill $skill
 * @property integer $skill_id
 * @property integer $person_id
 */
class PersonSkill extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'person_skill';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'skill_id',
        'person_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'skill_id' => 'integer',
        'person_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'created_at' => 'nullable',
//        'updated_at' => 'nullable',
//        'deleted_at' => 'nullable',
//        'skill_id' => 'required|integer',
//        'person_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function person()
    {
        return $this->belongsTo(\App\Models\Person::class, 'person_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function skill()
    {
        return $this->belongsTo(\App\Models\Skill::class, 'skill_id');
    }
}
