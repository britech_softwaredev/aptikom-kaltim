<?php

namespace App\Repositories;

use App\Models\Jabatan;
use App\Repositories\BaseRepository;

/**
 * Class JabatanRepository
 * @package App\Repositories
 * @version June 11, 2021, 1:32 am UTC
*/

class JabatanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'definition',
        'order',
        'show_in_header',
        'left',
        'right'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Jabatan::class;
    }
}
