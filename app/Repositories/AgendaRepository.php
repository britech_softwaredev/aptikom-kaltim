<?php

namespace App\Repositories;

use App\Models\Agenda;
use App\Repositories\BaseRepository;

/**
 * Class AgendaRepository
 * @package App\Repositories
 * @version March 15, 2021, 6:49 am UTC
*/

class AgendaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'content',
        'users_created_id',
        'users_updated_id',
        'slug',
        'agenda_category_id',
        'schedule_date',
        'schedule_time'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agenda::class;
    }
}
