<?php

namespace App\Repositories;

use App\Models\PersonHasPendidikan;
use App\Repositories\BaseRepository;

/**
 * Class PersonHasPendidikanRepository
 * @package App\Repositories
 * @version June 11, 2021, 1:33 am UTC
*/

class PersonHasPendidikanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'person_id',
        'pendidikan_id',
        'perguruan_tinggi',
        'program_studi',
        'tahun_lulus',
        'person_has_pendidikancol'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PersonHasPendidikan::class;
    }
}
