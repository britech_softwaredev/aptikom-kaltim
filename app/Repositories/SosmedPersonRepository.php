<?php

namespace App\Repositories;

use App\Models\SosmedPerson;
use App\Repositories\BaseRepository;

/**
 * Class SosmedPersonRepository
 * @package App\Repositories
 * @version December 9, 2021, 8:30 am UTC
*/

class SosmedPersonRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'value',
        'person_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SosmedPerson::class;
    }
}
