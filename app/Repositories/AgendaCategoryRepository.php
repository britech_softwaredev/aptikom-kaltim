<?php

namespace App\Repositories;

use App\Models\AgendaCategory;
use App\Repositories\BaseRepository;

/**
 * Class AgendaCategoryRepository
 * @package App\Repositories
 * @version March 15, 2021, 6:50 am UTC
*/

class AgendaCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'definition',
        'parent_id',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AgendaCategory::class;
    }
}
