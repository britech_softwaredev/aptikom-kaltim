<?php

namespace App\Repositories;

use App\Models\Galeri;
use App\Repositories\BaseRepository;

/**
 * Class GaleriRepository
 * @package App\Repositories
 * @version March 14, 2021, 12:57 am UTC
*/

class GaleriRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'content',
        'users_created_id',
        'users_updated_id',
        'slug',
        'event_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Galeri::class;
    }
}
