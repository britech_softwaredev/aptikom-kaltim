<?php

namespace App\Repositories;

use App\Models\PersonSkill;
use App\Repositories\BaseRepository;

/**
 * Class PersonSkillRepository
 * @package App\Repositories
 * @version December 9, 2021, 8:29 am UTC
*/

class PersonSkillRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'skill_id',
        'person_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PersonSkill::class;
    }
}
