<?php

namespace App\Repositories;

use App\Models\Pendidikan;
use App\Repositories\BaseRepository;

/**
 * Class PendidikanRepository
 * @package App\Repositories
 * @version March 16, 2021, 7:53 am UTC
*/

class PendidikanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'definition'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pendidikan::class;
    }
}
