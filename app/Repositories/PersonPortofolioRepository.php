<?php

namespace App\Repositories;

use App\Models\PersonPortofolio;
use App\Repositories\BaseRepository;

/**
 * Class PersonPortofolioRepository
 * @package App\Repositories
 * @version December 9, 2021, 8:34 am UTC
*/

class PersonPortofolioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'year',
        'company',
        'link',
        'person_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PersonPortofolio::class;
    }
}
