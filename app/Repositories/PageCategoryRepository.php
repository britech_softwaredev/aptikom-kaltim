<?php

namespace App\Repositories;

use App\Models\PageCategory;
use App\Repositories\BaseRepository;

/**
 * Class PageCategoryRepository
 * @package App\Repositories
 * @version March 15, 2021, 6:08 am UTC
*/

class PageCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'definition',
        'parent_id',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PageCategory::class;
    }
}
