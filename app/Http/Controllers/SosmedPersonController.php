<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSosmedPersonRequest;
use App\Http\Requests\UpdateSosmedPersonRequest;
use App\Models\Person;
use App\Models\SosmedPerson;
use App\Repositories\SosmedPersonRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SosmedPersonController extends AppBaseController
{
    /** @var  SosmedPersonRepository */
    private $sosmedPersonRepository;

    public function __construct(SosmedPersonRepository $sosmedPersonRepo)
    {
        $this->sosmedPersonRepository = $sosmedPersonRepo;
    }

    /**
     * Display a listing of the SosmedPerson.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sosmedPeople = $this->sosmedPersonRepository->all();

        return view('sosmed_people.index')
            ->with('sosmedPeople', $sosmedPeople);
    }

    /**
     * Show the form for creating a new SosmedPerson.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $person_id = $request['person_id'];
        $person = Person::orderBy('name_full','asc')->pluck('name_full','id');

        $data = SosmedPerson::where('person_id',$person_id)->get();
        $sosmedPerson = '';

        return view('sosmed_people.create',compact('person','person_id','data','sosmedPerson'));
    }

    /**
     * Store a newly created SosmedPerson in storage.
     *
     * @param CreateSosmedPersonRequest $request
     *
     * @return Response
     */
    public function store(CreateSosmedPersonRequest $request)
    {
        $input = $request->all();

        $sosmedPerson = $this->sosmedPersonRepository->create($input);

        Flash::success('Sosmed Person saved successfully.');

        return redirect(url()->previous());
    }

    /**
     * Display the specified SosmedPerson.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sosmedPerson = $this->sosmedPersonRepository->find($id);

        if (empty($sosmedPerson)) {
            Flash::error('Sosmed Person not found');

            return redirect(route('sosmedPeople.index'));
        }

        return view('sosmed_people.show')->with('sosmedPerson', $sosmedPerson);
    }

    /**
     * Show the form for editing the specified SosmedPerson.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sosmedPerson = $this->sosmedPersonRepository->find($id);

        if (empty($sosmedPerson)) {
            Flash::error('Sosmed Person not found');

            return redirect(route('sosmedPeople.index'));
        }

        return view('sosmed_people.edit')->with('sosmedPerson', $sosmedPerson);
    }

    /**
     * Update the specified SosmedPerson in storage.
     *
     * @param int $id
     * @param UpdateSosmedPersonRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSosmedPersonRequest $request)
    {
        $sosmedPerson = $this->sosmedPersonRepository->find($id);

        if (empty($sosmedPerson)) {
            Flash::error('Sosmed Person not found');

            return redirect(route('sosmedPeople.index'));
        }

        $sosmedPerson = $this->sosmedPersonRepository->update($request->all(), $id);

        Flash::success('Sosmed Person updated successfully.');

//        return redirect(route('sosmedPeople.index'));
        return redirect(url()->previous());
    }

    /**
     * Remove the specified SosmedPerson from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sosmedPerson = $this->sosmedPersonRepository->find($id);

        if (empty($sosmedPerson)) {
            Flash::error('Sosmed Person not found');

            return redirect(route('sosmedPeople.index'));
        }

        $this->sosmedPersonRepository->delete($id);

        Flash::success('Sosmed Person deleted successfully.');

        return redirect(url()->previous());
    }
}
