<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePersonPortofolioRequest;
use App\Http\Requests\UpdatePersonPortofolioRequest;
use App\Models\Person;
use App\Models\PersonPortofolio;
use App\Repositories\PersonPortofolioRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PersonPortofolioController extends AppBaseController
{
    /** @var  PersonPortofolioRepository */
    private $personPortofolioRepository;

    public function __construct(PersonPortofolioRepository $personPortofolioRepo)
    {
        $this->personPortofolioRepository = $personPortofolioRepo;
    }

    /**
     * Display a listing of the PersonPortofolio.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $person_id = $request['person_id'];
        $personPortofolios = PersonPortofolio::where('person_id',$request['person_id'])->get();

        return view('person_portofolios.index',compact('person_id'))
            ->with('personPortofolios', $personPortofolios);
    }

    /**
     * Show the form for creating a new PersonPortofolio.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $person_id = $request['person_id'];

        $data = PersonPortofolio::where('person_id',$person_id)->get();
        $person = Person::orderBy('name_full','asc')->pluck('name_full','id');

        return view('person_portofolios.create',compact('person','person_id','data'));
    }

    /**
     * Store a newly created PersonPortofolio in storage.
     *
     * @param CreatePersonPortofolioRequest $request
     *
     * @return Response
     */
    public function store(CreatePersonPortofolioRequest $request)
    {
        $input = $request->all();

        $personPortofolio = $this->personPortofolioRepository->create($input);

        Flash::success('Person Portofolio saved successfully.');

        return redirect(url()->previous());
    }

    /**
     * Display the specified PersonPortofolio.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $personPortofolio = $this->personPortofolioRepository->find($id);

        if (empty($personPortofolio)) {
            Flash::error('Person Portofolio not found');

            return redirect(route('personPortofolios.index'));
        }

        return view('person_portofolios.show')->with('personPortofolio', $personPortofolio);
    }

    /**
     * Show the form for editing the specified PersonPortofolio.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $personPortofolio = $this->personPortofolioRepository->find($id);

        if (empty($personPortofolio)) {
            Flash::error('Person Portofolio not found');

            return redirect(route('personPortofolios.index'));
        }

        return view('person_portofolios.edit')->with('personPortofolio', $personPortofolio);
    }

    /**
     * Update the specified PersonPortofolio in storage.
     *
     * @param int $id
     * @param UpdatePersonPortofolioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePersonPortofolioRequest $request)
    {
        $personPortofolio = $this->personPortofolioRepository->find($id);

        if (empty($personPortofolio)) {
            Flash::error('Person Portofolio not found');

            return redirect(route('personPortofolios.index'));
        }

        $personPortofolio = $this->personPortofolioRepository->update($request->all(), $id);

        Flash::success('Person Portofolio updated successfully.');

        return redirect(url()->previous());
    }

    /**
     * Remove the specified PersonPortofolio from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $personPortofolio = $this->personPortofolioRepository->find($id);

        if (empty($personPortofolio)) {
            Flash::error('Person Portofolio not found');

            return redirect(route('personPortofolios.index'));
        }

        $this->personPortofolioRepository->delete($id);

        Flash::success('Person Portofolio deleted successfully.');

        return redirect(url()->previous());
    }
}
