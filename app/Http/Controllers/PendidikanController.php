<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePendidikanRequest;
use App\Http\Requests\UpdatePendidikanRequest;
use App\Repositories\PendidikanRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PendidikanController extends AppBaseController
{
    /** @var  PendidikanRepository */
    private $pendidikanRepository;

    public function __construct(PendidikanRepository $pendidikanRepo)
    {
        $this->pendidikanRepository = $pendidikanRepo;
    }

    /**
     * Display a listing of the Pendidikan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->middleware(['auth','permissions:pendidikans.index']);
        $pendidikans = $this->pendidikanRepository->all();

        return view('pendidikans.index')
            ->with('pendidikans', $pendidikans);
    }

    /**
     * Show the form for creating a new Pendidikan.
     *
     * @return Response
     */
    public function create()
    {
        $this->middleware(['auth','permissions:pendidikans.create']);
        return view('pendidikans.create');
    }

    /**
     * Store a newly created Pendidikan in storage.
     *
     * @param CreatePendidikanRequest $request
     *
     * @return Response
     */
    public function store(CreatePendidikanRequest $request)
    {
        $this->middleware(['auth','permissions:pendidikans.store']);
        $input = $request->all();

        $pendidikan = $this->pendidikanRepository->create($input);

        Flash::success('Pendidikan saved successfully.');

        return redirect(route('pendidikans.index'));
    }

    /**
     * Display the specified Pendidikan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pendidikan = $this->pendidikanRepository->find($id);

        if (empty($pendidikan)) {
            Flash::error('Pendidikan not found');

            return redirect(route('pendidikans.index'));
        }

        return view('pendidikans.show')->with('pendidikan', $pendidikan);
    }

    /**
     * Show the form for editing the specified Pendidikan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->middleware(['auth','permissions:pendidikans.edit']);
        $pendidikan = $this->pendidikanRepository->find($id);

        if (empty($pendidikan)) {
            Flash::error('Pendidikan not found');

            return redirect(route('pendidikans.index'));
        }

        return view('pendidikans.edit')->with('pendidikan', $pendidikan);
    }

    /**
     * Update the specified Pendidikan in storage.
     *
     * @param int $id
     * @param UpdatePendidikanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePendidikanRequest $request)
    {
        $this->middleware(['auth','permissions:pendidikans.update']);
        $pendidikan = $this->pendidikanRepository->find($id);

        if (empty($pendidikan)) {
            Flash::error('Pendidikan not found');

            return redirect(route('pendidikans.index'));
        }

        $pendidikan = $this->pendidikanRepository->update($request->all(), $id);

        Flash::success('Pendidikan updated successfully.');

        return redirect(route('pendidikans.index'));
    }

    /**
     * Remove the specified Pendidikan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->middleware(['auth','permissions:pendidikans.destroy']);
        $pendidikan = $this->pendidikanRepository->find($id);

        if (empty($pendidikan)) {
            Flash::error('Pendidikan not found');

            return redirect(route('pendidikans.index'));
        }

        $this->pendidikanRepository->delete($id);

        Flash::success('Pendidikan deleted successfully.');

        return redirect(route('pendidikans.index'));
    }
}
