<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePersonSkillRequest;
use App\Http\Requests\UpdatePersonSkillRequest;
use App\Models\Person;
use App\Models\PersonSkill;
use App\Models\Skill;
use App\Repositories\PersonSkillRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PersonSkillController extends AppBaseController
{
    /** @var  PersonSkillRepository */
    private $personSkillRepository;

    public function __construct(PersonSkillRepository $personSkillRepo)
    {
        $this->personSkillRepository = $personSkillRepo;
    }

    /**
     * Display a listing of the PersonSkill.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $person_id = $request['id_person'];
        $personSkills = $this->personSkillRepository->all();

        return view('person_skills.index',compact('person_id'))
            ->with('personSkills', $personSkills);
    }

    /**
     * Show the form for creating a new PersonSkill.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $person_id = $request['person_id'];
        $skill = Skill::orderBy('name','asc')->pluck('name','id');
        $person = Person::orderBy('name_full','asc')->pluck('name_full','id');

        $data = PersonSkill::where('person_id',$person_id)->get();
        $personSkill = '';

        return view('person_skills.create',compact('skill','person','person_id','data','personSkill'));
    }

    /**
     * Store a newly created PersonSkill in storage.
     *
     * @param CreatePersonSkillRequest $request
     *
     * @return Response
     */
    public function store(CreatePersonSkillRequest $request)
    {
        $input = $request->all();

        $personSkill = $this->personSkillRepository->create($input);

        Flash::success('Person Skill saved successfully.');

        return redirect(url()->previous());
    }

    /**
     * Display the specified PersonSkill.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $personSkill = $this->personSkillRepository->find($id);

        if (empty($personSkill)) {
            Flash::error('Person Skill not found');

            return redirect(route('personSkills.index'));
        }

        return view('person_skills.show')->with('personSkill', $personSkill);
    }

    /**
     * Show the form for editing the specified PersonSkill.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $skill = Skill::orderBy('name','asc')->pluck('name','id');
        $person = Person::orderBy('name_full','asc')->pluck('name_full','id');

        $personSkill = $this->personSkillRepository->find($id);

        if (empty($personSkill)) {
            Flash::error('Person Skill not found');

            return redirect(route('personSkills.index'));
        }

        return view('person_skills.edit',compact('skill','person'))->with('personSkill', $personSkill);
    }

    /**
     * Update the specified PersonSkill in storage.
     *
     * @param int $id
     * @param UpdatePersonSkillRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePersonSkillRequest $request)
    {
        $personSkill = $this->personSkillRepository->find($id);

        if (empty($personSkill)) {
            Flash::error('Person Skill not found');

            return redirect(route('personSkills.index'));
        }

        $personSkill = $this->personSkillRepository->update($request->all(), $id);

        Flash::success('Person Skill updated successfully.');

        return redirect(url()->previous());

//        return redirect(route('personSkills.index'));
    }

    /**
     * Remove the specified PersonSkill from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $personSkill = $this->personSkillRepository->find($id);

        if (empty($personSkill)) {
            Flash::error('Person Skill not found');

            return redirect(route('personSkills.index'));
        }

        $this->personSkillRepository->delete($id);

        Flash::success('Person Skill deleted successfully.');

        return redirect(url()->previous());
    }
}
