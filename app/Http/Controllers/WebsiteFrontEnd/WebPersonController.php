<?php

namespace App\Http\Controllers\WebsiteFrontEnd;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\WebsiteFrontEnd\Utility\DataStaticController;
use App\Models\Person;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Http\Request;
use Flash;
use Response;

class WebPersonController extends AppBaseController
{


    /**
     * Display a listing of the PostCategory.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function person() {

        $title = "Tim Pendukung";

        SEOTools::setTitle($title);
        SEOTools::setDescription('Show '.$title);
        SEOTools::opengraph()->addProperty('type', 'home');

        $person = Person::orderBy('order')->get();

        return view('website.person.person',compact('person'));
    }

    public function personDetail($slug) {

        $title = "Tim Pendukung";

        SEOTools::setTitle($title);
        SEOTools::setDescription('Show '.$title);
        SEOTools::opengraph()->addProperty('type', 'home');

        $person = Person::where('slug',$slug)->first();

        return view('website.person.person-detail',compact('person'));
    }
//    public function akdListPerson($slugAkdCategory)
//    {
//        $akdCategory=AkdCategory::where('slug',$slugAkdCategory)->first();
//
//        if(empty($akdCategory)){
//            return redirect(route('beranda'));
//        }
//
//        $title="Anggota Alat Kelengkapan Dewan ".$akdCategory->name;
//
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title." - ".$akdCategory->definition);
//        SEOTools::opengraph()->addProperty('type', 'profile');
//
//        $listJabatan = JabatanDewan::whereHas('AkdPersonDewan',function ($q)use($akdCategory){
//            $q->where('akd_category_id',$akdCategory->id);
//        })->orderBy('order')->get();
//
//        $listPersonDewan=[];
//
//        foreach ($listJabatan as $jabatan){
//            $akdPersons=AkdPersonDewan::where('jabatan_dewan_id',$jabatan->id)
//                ->where('akd_category_id',$akdCategory->id)->get();
//
//            array_push($listPersonDewan,$akdPersons);
//        }
//
//        return view('web_frontend.person.list_akd_person',compact('akdCategory','listJabatan','listPersonDewan'));
//    }
//
//    public function listPersonDewan()
//    {
//        $listDapil=DaerahPemilihan::orderBy('order')->get();
//        $title="Anggota DPRD Provinsi Kalimantan Timur";
//
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title);
//        SEOTools::opengraph()->addProperty('type', 'profile');
//
//        return view('web_frontend.person.list_person_dewan',compact('listDapil'));
//    }
//
//    public function listPersonSekretariat()
//    {
//        $maxOrder=JabatanSekretariat::max('order');
//        $minOrder=JabatanSekretariat::min('order');
//
//        $listJabatan=[];
//        for ($i=$minOrder;$i<=$maxOrder;$i++){
//            array_push($listJabatan,JabatanSekretariat::where('order',$i)->get());
//        }
//
//        $title="Anggota Sekretariat DPRD Provinsi Kalimantan Timur";
//
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title);
//        SEOTools::opengraph()->addProperty('type', 'profile');
//
//        return view('web_frontend.person.list_person_sekretariat',compact('listJabatan'));
//    }
//
//    public function detailPersonDewan($slug) {
//        $listDapil=DaerahPemilihan::orderBy('order')->get();
//        $person = PersonDewan::where('slug',$slug)->first();
//        $title="Profile ".$person->name;
//
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title);
//        SEOTools::opengraph()->addProperty('type', 'profile');
//
//        return view('web_frontend.person.detail_person_dewan',compact('title','person','listDapil'));
//    }
//
//    public function detailPersonSekretariat($slug) {
//        $person = PersonSekretariat::where('slug',$slug)->first();
//        $title="Profile ".$person->name;
//
//        SEOTools::setTitle($title);
//        SEOTools::setDescription('Show '.$title);
//        SEOTools::opengraph()->addProperty('type', 'profile');
//
//        return view('web_frontend.person.detail_person_sekretariat',compact('title','person'));
//    }

}
