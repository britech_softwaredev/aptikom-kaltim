<?php


namespace App\Http\Controllers\WebsiteFrontEnd\Utility;


use App\Http\Controllers\Controller;
use App\Models\AkdCategory;
use App\Models\AkdPersonDewan;
use App\Models\PageCategory;
use App\Models\PersonSekretariat;
use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class DataStaticController extends Controller
{
    public function __construct()
    {
        $listPost = Cache::remember("post-latest",360 , function (){
            return Post::latest()->take(6)->get();
        });

        $postCategory = Cache::remember("postCategory" ,3600, function () {
            return PostCategory::whereNull('parent_id')->orderBy('order')->get();
        });

        $postCategorys = Cache::remember("postCategorys" ,3600, function () {
            return PostCategory::whereNotNull('parent_id')->orderBy('order')->get();
        });

        $pageCategory= Cache::remember("pageCategory" , 3600,function () {
            return PageCategory::whereNull('parent_id')->orderBy('order')->get();
        });

        $headerPerson= Cache::remember("person-dewan-in-header",360 , function (){
            return AkdPersonDewan::whereHas("akdCategory",function ($q){
                $q->where("show_in_header",1);
            })->whereHas("jabatanDewan",function ($q){
                $q->where("show_in_header",1);
            })->orderBy("order_in_header")->get();
        });

        $headerSekretariat= Cache::remember("person-sekretariat-in-header",360 , function (){
            return PersonSekretariat::whereHas("jabatanSekretariat",function ($q){
                $q->where("show_in_header",1);
            })->get();
        });

        return View::share(compact('listPost','pageCategory','postCategory','headerPerson','headerSekretariat','postCategorys'));
    }
}
