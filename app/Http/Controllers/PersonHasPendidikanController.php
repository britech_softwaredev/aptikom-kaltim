<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePersonHasPendidikanRequest;
use App\Http\Requests\UpdatePersonHasPendidikanRequest;
use App\Models\Pendidikan;
use App\Models\PersonHasPendidikan;
use App\Repositories\PersonHasPendidikanRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PersonHasPendidikanController extends AppBaseController
{
    /** @var  PersonHasPendidikanRepository */
    private $personHasPendidikanRepository;

    public function __construct(PersonHasPendidikanRepository $personHasPendidikanRepo)
    {
        $this->personHasPendidikanRepository = $personHasPendidikanRepo;
    }

    /**
     * Display a listing of the PersonHasPendidikan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $personHasPendidikans = $this->personHasPendidikanRepository->all();

        return view('person_has_pendidikans.index')
            ->with('personHasPendidikans', $personHasPendidikans);
    }

    /**
     * Show the form for creating a new PersonHasPendidikan.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $person_id = $request['person_id'];
        $pendidikan = Pendidikan::pluck('name','id');

        $data = PersonHasPendidikan::where('person_id',$person_id)->get();

        return view('person_has_pendidikans.create',compact('pendidikan','person_id','data'));
    }

    /**
     * Store a newly created PersonHasPendidikan in storage.
     *
     * @param CreatePersonHasPendidikanRequest $request
     *
     * @return Response
     */
    public function store(CreatePersonHasPendidikanRequest $request)
    {
        $input = $request->all();

        $personHasPendidikan = $this->personHasPendidikanRepository->create($input);

        Flash::success('Person Has Pendidikan saved successfully.');

//        return redirect(route('personHasPendidikans.index'));
        return redirect(url()->previous());

    }

    /**
     * Display the specified PersonHasPendidikan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $personHasPendidikan = $this->personHasPendidikanRepository->find($id);

        if (empty($personHasPendidikan)) {
            Flash::error('Person Has Pendidikan not found');

            return redirect(route('personHasPendidikans.index'));
        }

        return view('person_has_pendidikans.show')->with('personHasPendidikan', $personHasPendidikan);
    }

    /**
     * Show the form for editing the specified PersonHasPendidikan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $person_id = $request['person_id'];
        $pendidikan = Pendidikan::pluck('name','id');

        $personHasPendidikan = $this->personHasPendidikanRepository->find($id);

        if (empty($personHasPendidikan)) {
            Flash::error('Person Has Pendidikan not found');

            return redirect(route('personHasPendidikans.index'));
        }

        return view('person_has_pendidikans.edit',compact('person_id','pendidikan'))->with('personHasPendidikan', $personHasPendidikan);
    }

    /**
     * Update the specified PersonHasPendidikan in storage.
     *
     * @param int $id
     * @param UpdatePersonHasPendidikanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePersonHasPendidikanRequest $request)
    {
        $personHasPendidikan = $this->personHasPendidikanRepository->find($id);

        if (empty($personHasPendidikan)) {
            Flash::error('Person Has Pendidikan not found');

            return redirect(route('personHasPendidikans.index'));
        }

        $personHasPendidikan = $this->personHasPendidikanRepository->update($request->all(), $id);

        Flash::success('Person Has Pendidikan updated successfully.');

//        return redirect(route('personHasPendidikans.index'));
        return redirect(url()->previous());

    }

    /**
     * Remove the specified PersonHasPendidikan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $personHasPendidikan = $this->personHasPendidikanRepository->find($id);

        if (empty($personHasPendidikan)) {
            Flash::error('Person Has Pendidikan not found');

            return redirect(route('personHasPendidikans.index'));
        }

        $this->personHasPendidikanRepository->delete($id);

        Flash::success('Person Has Pendidikan deleted successfully.');

//        return redirect(route('personHasPendidikans.index'));
        return redirect(url()->previous());
    }
}
