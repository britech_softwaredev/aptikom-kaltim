<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePersonRequest;
use App\Http\Requests\UpdatePersonRequest;
use App\Models\Jabatan;
use App\Models\Person;
use App\Models\PersonHasPendidikan;
use App\Models\Role;
use App\Models\User;
use App\Repositories\PersonRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Hash;
use Matrix\Exception;
use Response;

class PersonController extends AppBaseController
{
    /** @var  PersonRepository */
    private $personRepository;

    public function __construct(PersonRepository $personRepo)
    {
        $this->personRepository = $personRepo;
    }

    /**
     * Display a listing of the Person.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $people = Person::orderBy('no_anggota','asc')->get();

        return view('people.index')
            ->with('people', $people);
    }

    /**
     * Show the form for creating a new Person.
     *
     * @return Response
     */
    public function create()
    {
//        $jabatan=Jabatan::pluck('name','id');
        return view('people.create');
    }

    /**
     * Store a newly created Person in storage.
     *
     * @param CreatePersonRequest $request
     *
     * @return Response
     */
    public function store(CreatePersonRequest $request)
    {
        $input = $request->all();

        $person = $this->personRepository->create($input);
        $person->addFromMediaLibraryRequest($request->media)
            ->toMediaCollection();

        Flash::success('Person saved successfully.');

        return redirect(route('people.index'));
    }

    /**
     * Display the specified Person.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $person = $this->personRepository->find($id);

        if (empty($person)) {
            Flash::error('Person not found');

            return redirect(route('people.index'));
        }

        return view('people.show')->with('person', $person);
    }

    /**
     * Show the form for editing the specified Person.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $person = $this->personRepository->find($id);

        if (empty($person)) {
            Flash::error('Person not found');

            return redirect(route('people.index'));
        }
//        $jabatan=Jabatan::pluck('name','id');
        return view('people.edit')->with('person', $person);
    }

    /**
     * Update the specified Person in storage.
     *
     * @param int $id
     * @param UpdatePersonRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePersonRequest $request)
    {
        $person = $this->personRepository->find($id);

        if (empty($person)) {
            Flash::error('Person not found');

            return redirect(route('people.index'));
        }

        $person = $this->personRepository->update($request->all(), $id);
        $person->syncFromMediaLibraryRequest($request->media)
            ->toMediaCollection();

        Flash::success('Person updated successfully.');

        return redirect(route('people.index'));
    }

    /**
     * Remove the specified Person from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $person = $this->personRepository->find($id);

        if (empty($person)) {
            Flash::error('Person not found');

            return redirect(route('people.index'));
        }

        $this->personRepository->delete($id);

        Flash::success('Person deleted successfully.');

        return redirect(route('people.index'));
    }

    public function synchronize() {
        try {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://api-gateway.aptikom.or.id/api/anggota-kaltim',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('_token' => '$2y$10$ueORlh6m898C8TaRkQQ7pu3NSl80A6EGJjdQSB0EpQyfCO2dcNntG'),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $data = json_decode($response, true);

            if ($data['message'] = 'Success') {

                foreach ($data['data'] as $index => $item) {

                    if ($item['email'] == "") {
                        $emails = str_replace(" ","_",$item['nama']).'@gmail.com';
                    } else {
                        $emails = $item['email'];
                    }

                    if (User::where('email','=',$emails)->exists()) {
                        if ($index == 0) Flash::success('Synchronize Successfully');
                    } else {
                        $name = str_replace(str_split('\\/:*?"<>|+-.'),"_",$item['nama'].'_'.$item['no_anggota']);
                        $user['name'] = $name;
                        $user['display_name'] = $item['nama'];
                        $user['email'] = $emails;
                        $user['password'] = Hash::make("1234567890");

                        $users = User::create($user);

                        $role = Role::select("id")->where("name", "anggota")->first();
                        $users->roles()->attach($role);

                        Person::create([
                            'users_id' => $users->id,
                            'no_anggota' => $item['no_anggota'],
                            'persone' => $item['persone'],
                            'name_full' => $item['nama'],
                            'masa_berlaku' => $item['masa_berlaku'],
                            'no_hp' => $item['no_hp'],
                            'email_valid' => $item['email'],
                            'alamat_perguruan_tinggi' => $item['alamat_perguruan_tinggi'],
                            'jabatan_perguruan_tinggi' => $item['jabatan'],
                            'program_studi' => $item['program_studi'],
                            'perguruan_tinggi' => $item['perguruan_tinggi'],
                        ]);

                        if ($index == 0) Flash::success('Synchronize Successfully');
                    }
                }
            } else {
                Flash::info('Data Tidak Ada');
            }
            return redirect(route('people.index'));
        } catch (Exception $e) {
            Flash::warning($e);
        }
    }
}
