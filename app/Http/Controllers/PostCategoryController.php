<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostCategoryRequest;
use App\Http\Requests\UpdatePostCategoryRequest;
use App\Models\PostCategory;
use App\Repositories\PostCategoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Cache;
use Response;

class PostCategoryController extends AppBaseController
{
    /** @var  PostCategoryRepository */
    private $postCategoryRepository;

    public function __construct(PostCategoryRepository $postCategoryRepo)
    {
        $this->postCategoryRepository = $postCategoryRepo;
    }

    /**
     * Display a listing of the PostCategory.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $postCategories = PostCategory::whereNull('parent_id')->orderBy('order')->withTrashed()->get();

        return view('post_categories.index')
            ->with('postCategories', $postCategories);
    }

    /**
     * Show the form for creating a new PostCategory.
     *
     * @return Response
     */
    public function create()
    {
        $postCategories= \App\Models\PostCategory::whereNull('parent_id')->pluck('name','id');
        return view('post_categories.create',compact('postCategories'));
    }

    /**
     * Store a newly created PostCategory in storage.
     *
     * @param CreatePostCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreatePostCategoryRequest $request)
    {
        $input = $request->all();

        $postCategory = $this->postCategoryRepository->create($input);
        Cache::forget('home_post_category');
        Flash::success('Post Category saved successfully.');

        return redirect(route('postCategories.index'));
    }

    /**
     * Display the specified PostCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $postCategory = $this->postCategoryRepository->find($id);

        if (empty($postCategory)) {
            Flash::error('Post Category not found');

            return redirect(route('postCategories.index'));
        }

        return view('post_categories.show')->with('postCategory', $postCategory);
    }

    /**
     * Show the form for editing the specified PostCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $postCategory = $this->postCategoryRepository->find($id);

        if (empty($postCategory)) {
            Flash::error('Post Category not found');

            return redirect(route('postCategories.index'));
        }
        $postCategories= \App\Models\PostCategory::whereNull('parent_id')->where('id','<>',$id)->pluck('name','id');
        return view('post_categories.edit',compact('postCategories'))->with('postCategory', $postCategory);
    }

    /**
     * Update the specified PostCategory in storage.
     *
     * @param int $id
     * @param UpdatePostCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostCategoryRequest $request)
    {
        $postCategory = $this->postCategoryRepository->find($id);

        if (empty($postCategory)) {
            Flash::error('Post Category not found');

            return redirect(route('postCategories.index'));
        }

        $postCategory = $this->postCategoryRepository->update($request->all(), $id);
        Cache::forget('home_post_category');
        Flash::success('Post Category updated successfully.');

        return redirect(route('postCategories.index'));
    }

    /**
     * Remove the specified PostCategory from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $postCategory = PostCategory::withTrashed()->find($id);

        if (empty($postCategory)) {
            Flash::error('Post Category not found');

            return redirect(route('postCategories.index'));
        }

        if(!$postCategory->trashed()){
            $this->postCategoryRepository->delete($id);
        }else{
            $postCategory->restore();
        }
        Cache::forget('home_post_category');

        Flash::success('Post Category deleted successfully.');

        return redirect(route('postCategories.index'));
    }

    public function increase($id)
    {
        $postCategory = PostCategory::withTrashed()->find($id);

        if (empty($postCategory)) {
            Flash::error('Post Category not found');
            return redirect(route('postCategories.index'));
        }

        if($postCategory->order>0){
            $postCategory->order=$postCategory->order-1;
            $postCategory->save();

            Cache::forget('home_post_category');
            Flash::success('Urutan Berhasil Naik');
        }else{
            Flash::error('Urutan mencapai nilai batas');
        }

        return redirect(route('postCategories.index'));
    }

    public function decrease($id)
    {
        $postCategory = PostCategory::withTrashed()->find($id);

        if (empty($postCategory)) {
            Flash::error('Post Category not found');
            return redirect(route('postCategories.index'));
        }

        $postCategory->order=$postCategory->order+1;
        $postCategory->save();
        Cache::forget('home_post_category');
        Flash::success('Urutan Berhasil Turun');

        return redirect(route('postCategories.index'));
    }
}
