<?php

namespace App\Http\Livewire;

use App\Models\Portofolio;
use App\Models\Post;
use App\Models\PostCategory;
use Livewire\Component;

class ShowPortofolio extends Component
{

    public $listPortofolio;
    public $idCategory=0;
    public $activeAll = "";

    public function render()
    {
        if($this->idCategory===0){
            $this->activeAll = "active";
            $this->listPortofolio = Portofolio::latest()->limit(6)->get();
        }else{
            $this->activeAll = "";
            $this->listPortofolio = Portofolio::where('jenis_portofolio_id', $this->idCategory)
                ->latest()->limit(6)->get();
        }

        return view('livewire.show-portofolio');
    }

    public function showPosts($id_category)
    {
        $this->idCategory=$id_category;
    }

    public function allCategories()
    {
        $this->idCategory=0;
    }
}
