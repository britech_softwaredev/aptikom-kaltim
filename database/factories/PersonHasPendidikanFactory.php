<?php

namespace Database\Factories;

use App\Models\PersonHasPendidikan;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonHasPendidikanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PersonHasPendidikan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'person_id' => $this->faker->word,
        'pendidikan_id' => $this->faker->word,
        'perguruan_tinggi' => $this->faker->word,
        'program_studi' => $this->faker->word,
        'tahun_lulus' => $this->faker->word,
        'person_has_pendidikancol' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
