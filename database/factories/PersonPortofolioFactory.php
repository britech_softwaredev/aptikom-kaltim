<?php

namespace Database\Factories;

use App\Models\PersonPortofolio;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonPortofolioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PersonPortofolio::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'year' => $this->faker->word,
        'company' => $this->faker->word,
        'link' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'person_id' => $this->faker->word
        ];
    }
}
