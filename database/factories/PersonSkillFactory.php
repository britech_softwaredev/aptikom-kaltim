<?php

namespace Database\Factories;

use App\Models\PersonSkill;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonSkillFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PersonSkill::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'skill_id' => $this->faker->randomDigitNotNull,
        'person_id' => $this->faker->word
        ];
    }
}
