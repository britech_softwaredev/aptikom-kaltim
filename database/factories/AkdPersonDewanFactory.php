<?php

namespace Database\Factories;

use App\Models\AkdPersonDewan;
use Illuminate\Database\Eloquent\Factories\Factory;

class AkdPersonDewanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AkdPersonDewan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'person_dewan_id' => $this->faker->word,
        'jabatan_dewan_id' => $this->faker->word,
        'order' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'akd_category_id' => $this->faker->word
        ];
    }
}
