<?php

namespace Database\Factories;

use App\Models\AgendaCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class AgendaCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AgendaCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'definition' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'parent_id' => $this->faker->word,
        'slug' => $this->faker->word
        ];
    }
}
