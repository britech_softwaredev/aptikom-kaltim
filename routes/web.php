<?php

use App\Http\Controllers\PersonController;
use App\Http\Controllers\WebsiteFrontEnd\WebsiteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Route::get('/', function () {
//   return view('welcome');
//});

Auth::routes(['verify' => true,
    'register' => true,
    'reset' => false,
]);

Route::get('/refereshcaptcha', 'App\Http\Controllers\Auth\LoginController@refereshCaptcha');

//Custom Auth Routes
// Authentication Routes...

// Password Reset Routes...
/*$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');*/

Route::group(['middleware' => ['auth']], function () {

    Route::mediaLibrary();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('roles', App\Http\Controllers\RoleController::class);

    Route::resource('permissions', App\Http\Controllers\PermissionController::class);

    Route::resource('users', App\Http\Controllers\UserController::class);

    Route::get('profil', [App\Http\Controllers\UserController::class, 'profil'])->name('profil');
    Route::get('editProfil/{id}', [App\Http\Controllers\UserController::class, 'editProfiles']);
    Route::patch('updateProfile/{id}', [App\Http\Controllers\UserController::class, 'updateProfile']);

    Route::resource('galeris', App\Http\Controllers\GaleriController::class);


    Route::resource('posts', 'App\Http\Controllers\PostController');

    Route::resource('postCategories', 'App\Http\Controllers\PostCategoryController');
    Route::post('postCategoriesIncrease/{id}', 'App\Http\Controllers\PostCategoryController@increase')->name('postCategoriesIncrease');
    Route::post('postCategoriesDecrease/{id}', 'App\Http\Controllers\PostCategoryController@decrease')->name('postCategoriesDecrease');

    Route::resource('pendidikans', 'App\Http\Controllers\PendidikanController');

    Route::resource('profiles', 'App\Http\Controllers\ProfileController');
    Route::get('editProfiles/{id}', [\App\Http\Controllers\ProfileController::class,'editInfo']);

    Route::resource('portofolios', 'App\Http\Controllers\PortofolioController');

    Route::resource('people', 'App\Http\Controllers\PersonController');

    Route::resource('personHasPendidikans', 'App\Http\Controllers\PersonHasPendidikanController');

    Route::resource('skills', 'App\Http\Controllers\SkillController');

    Route::resource('personSkills', 'App\Http\Controllers\PersonSkillController');

    Route::resource('sosmedPeople', 'App\Http\Controllers\SosmedPersonController');

    Route::resource('personPortofolios', 'App\Http\Controllers\PersonPortofolioController');

    Route::resource('jenisPortofolios', 'App\Http\Controllers\JenisPortofolioController');

    Route::get('synchronize-anggota',[PersonController::class,'synchronize'])->name('synchronizeAnggota');
});

//----WEBSITE FRONT END
Route::get('/', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'index'])->name('beranda');
//

//
////Post Category
Route::get('galeri-foto', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'galeriFoto'])->name('galeri-foto');
Route::get('galeri-foto/{slug}', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'detailGaleriFoto'])->name('detail-galeri-foto');

Route::get('/',[WebsiteController::class,'index'])->name('beranda');
////Detail Post
Route::get('post/{slug}', [WebsiteController::class,'detailPost'])->name('detail-post');
Route::get('page/{slug}', [\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'detailPage'])->name('detail-page');

Route::get('person',[\App\Http\Controllers\WebsiteFrontEnd\WebPersonController::class,'person'])->name("persons");
Route::get('person/{slug}',[\App\Http\Controllers\WebsiteFrontEnd\WebPersonController::class,'personDetail'])->name("detail-person");
Route::get('portofolio',[WebsiteController::class,'portofolio'])->name('portofolio');
Route::get('mitra',[\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'mitra'])->name("mitra-britech");

Route::get('instagram',function (){
    $data=Dymantic\InstagramFeed\Profile::feed($limit = 20);
    return $data;

});
Route::get('tentang',[\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'tentang'])->name("tentang");
Route::get('pengurus',[\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'pengurus'])->name("pengurus");
Route::get('detail_pengurus/{slug}',[\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'detailPengurus'])->name("detailPengurus");
Route::get('caradaftar',[\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'caradaftar'])->name("caradaftar");
Route::get('daftar',[\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'daftar'])->name("daftar");
Route::get('anggota',[\App\Http\Controllers\WebsiteFrontEnd\WebsiteController::class,'anggota'])->name("anggota");
