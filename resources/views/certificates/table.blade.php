<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default table-responsive">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Name</th>
        <th>Definition</th>
        <th>Certificate Date</th>
        <th>Expired Date</th>
        <th>Institution</th>
        <th>Person Id</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($certificates as $certificate)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $certificate->name !!}</td>
            <td>{!! $certificate->definition !!}</td>
            <td>{!! $certificate->certificate_date !!}</td>
            <td>{!! $certificate->expired_date !!}</td>
            <td>{!! $certificate->institution !!}</td>
            <td>{!! $certificate->person_id !!}</td>
            <td>
                {!! Form::open(['route' => ['certificates.destroy', $certificate->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('certificates.show', [$certificate->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('certificates.edit', [$certificate->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
