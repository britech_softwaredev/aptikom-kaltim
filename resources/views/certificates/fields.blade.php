<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <div class="position-relative">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Definition Field -->
<div class="form-group">
    {!! Form::label('definition', 'Definition:') !!}
    <div class="position-relative">
        {!! Form::text('definition', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Certificate Date Field -->
<div class="form-group">
    {!! Form::label('certificate_date', 'Certificate Date:') !!}
    <div class="position-relative">
        {!! Form::date('certificate_date', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Expired Date Field -->
<div class="form-group">
    {!! Form::label('expired_date', 'Expired Date:') !!}
    <div class="position-relative">
        {!! Form::date('expired_date', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Institution Field -->
<div class="form-group">
    {!! Form::label('institution', 'Institution:') !!}
    <div class="position-relative">
        {!! Form::text('institution', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Person Id Field -->
<div class="form-group">
    {!! Form::label('person_id', 'Person Id:') !!}
    <div class="position-relative">
        {!! Form::number('person_id', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! route('certificates.index') !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

