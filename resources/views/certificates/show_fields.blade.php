<!-- Sudah di modifikasi -->
<!-- Name Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('name', 'Name:') !!}
        </h5>
        {!! $certificate->name !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Definition Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('definition', 'Definition:') !!}
        </h5>
        {!! $certificate->definition !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Certificate Date Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('certificate_date', 'Certificate Date:') !!}
        </h5>
        {!! $certificate->certificate_date !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Expired Date Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('expired_date', 'Expired Date:') !!}
        </h5>
        {!! $certificate->expired_date !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Institution Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('institution', 'Institution:') !!}
        </h5>
        {!! $certificate->institution !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Person Id Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('person_id', 'Person Id:') !!}
        </h5>
        {!! $certificate->person_id !!}
    </div>
</div>

