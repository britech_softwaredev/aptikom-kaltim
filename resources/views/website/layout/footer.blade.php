<footer class="footer black-bg">
    <div class="page-section-pt">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 sm-mb-30">
                    <div class="footer-logo">
                        <img id="logo-footer" class="mb-20" src="images/logo.png" alt="">
                        <p class="pb-10"> Asosiasi Pendidikan Tinggi Informatika dan Komputer</p>
                    </div>
                    <div class="social-icons color-hover">
                        <ul>
                            <li class="social-facebook"><a href="https://www.facebook.com/aptikom.kaltim"><i class="fa fa-facebook"></i></a></li>
                            <li class="social-instagram"><a href="https://www.instagram.com/pkn.desasumbersari2021/"><i class="fa fa-instagram"></i></a></li>
                            <li class="social-google"><a href="https://www.google.com/search?q=aptikom+kaltim&rlz=1C1CHBD_idID887ID890&hl=en&sxsrf=AOaemvJ3NbW-1otB-Xji8nJZmgjtIS_kbQ:1641279993512&source=lnms&sa=X&ved=2ahUKEwjIpfW3xJf1AhWxSmwGHVoFAfEQ_AUoAHoECAEQAg&biw=1366&bih=638&dpr=1"><i class="fa fa-google"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6 sm-mb-30">
                    <div class="footer-useful-link footer-hedding">
                        <h6 class="text-white mb-30 mt-10 text-uppercase">Navigation</h6>
                        <ul>
                            <li><a href="{{url('beranda')}}">Home</a></li>
                            <li><a href="{{url('tentang')}}">About Us</a></li>
                            <li><a href="{{url('pengurus')}}">Team</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <h6 class="text-white mb-30 mt-10 text-uppercase">Contact Us</h6>
                    <ul class="addresss-info">
                        <li><i class="fa fa-map-marker"></i>
                            <p>Address: Jln. Bung Tomo</p>
                        </li>
                        <li><i class="fa fa-phone"></i> <a href="tel:7042791249"> <span>+6289694338965 </span> </a>
                        </li>
                        <li><i class="fa fa-envelope-o"></i>Email: aptikomklatim@gmail.com</li>
                    </ul>
                </div>
            </div>
            <div class="footer-widget mt-20">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="mt-15"> &copy;Copyright <span id="copyright">
                                <script>
                                document.getElementById('copyright').appendChild(document.createTextNode(new Date()
                                    .getFullYear()))
                                </script>
                            </span> <a href="#"> Asosiasi Pendidikan Tinggi Informatika dan Komputer </a> Kalimantan Timur </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
