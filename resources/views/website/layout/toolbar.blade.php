<header id="header" class="header fancy">
    <div style="background-color: #1A8BCD" class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 xs-mb-10">
                    <div class="topbar-call text-center text-md-left">
                        <ul>
                            <li><i class="fa fa-envelope-o white"></i> aptikomkaltim@gmail.com</li>
                            <li><i class="fa fa-phone"></i> <a href="tel:+7042791249"> <span>+(704) 279-1249 </span>
                                </a> </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="topbar-social text-center text-md-right">
                        <ul>
                            <li><a href="https://www.facebook.com/aptikom.kaltim"><span class="ti-facebook"></span></a>
                            </li>
                            <li><a href="https://www.instagram.com/pkn.desasumbersari2021/"><span
                                        class="ti-instagram"></span></a></li>
                            <li><a
                                    href="https://www.google.com/search?q=aptikom+kaltim&rlz=1C1CHBD_idID887ID890&hl=en&sxsrf=AOaemvJ3NbW-1otB-Xji8nJZmgjtIS_kbQ:1641279993512&source=lnms&sa=X&ved=2ahUKEwjIpfW3xJf1AhWxSmwGHVoFAfEQ_AUoAHoECAEQAg&biw=1366&bih=638&dpr=1"><span
                                        class="ti-google"></span></a></li>
                            <li><a href="/login"><span>Login</span></a></li>
                            <!-- <li><a href="/register"><span>Register</span></a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--=================================
 mega menu -->

    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <!-- menu start -->
                    <nav id="menu" class="mega-menu">
                        <!-- menu list items container -->
                        <section class="menu-list-items">
                            <!-- menu logo -->
                            <ul class="menu-logo">
                                <li>
                                    <a href="index-01.html"><img id="logo_img" src="{{asset('image/download.png') }}"
                                            alt="logo"> </a>
                                </li>
                            </ul>
                            <!-- menu links -->
                            <div class="menu-bar">
                                <ul class="menu-links">

                                    <li><a href="{{url('/')}}">Beranda</a>
                                    </li>
                                    <li><a href="javascript:void(0)">Aptikom <i
                                                class="fa fa-angle-down fa-indicator"></i></a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="{{route('tentang')}}">Profil/Sejarah</a></li>
                                        </ul>
                                    </li>
                                    <!-- <li><a href="javascript:void(0)">Kerjasama <i
                                                class="fa fa-angle-down fa-indicator"></i></a> -->
                                    <!-- drop down multilevel  -->
                                    <!-- <ul class="drop-down-multilevel">
                                            <li><a href="{{route('pengurus')}}">Pemerintah</a></li>
                                            <li><a href="{{route('pengurus')}}">Perusahaan</a></li>
                                            <li><a href="{{route('pengurus')}}">Organisasi Kemasyarakatan</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="javascript:void(0)">Unduhan <i
                                                class="fa fa-angle-down fa-indicator"></i></a>
                                    </li> -->
                                    <li><a href="javascript:void(0)">Organisasi <i
                                                class="fa fa-angle-down fa-indicator"></i></a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="{{route('anggota')}}">Struktur Kepengurusan</a></li>
                                            <li><a href="{{route('pengurus')}}">Data Anggota Aptikom</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="javascript:void(0)">Keanggotaan <i
                                                class="fa fa-angle-down fa-indicator"></i></a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="{{route('daftar')}}">Pendaftaran </a></li>
                                            <li><a href="{{route('caradaftar')}}">Cara Pendaftaran </a></li>
                                        </ul>
                                    </li>
                                    <li><a href="javascript:void(0)">Publikasi <i
                                                class="fa fa-angle-down fa-indicator"></i></a>
                                        <!-- drop down multilevel  -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="{{route('galeri-foto')}}">Galeri Foto </a></li>
                                        </ul>
                                    </li>
                                    <!-- <li><a href="javascript:void(0)">Event<i class="fa fa-angle-down fa-indicator"></i></a>
               <ul class="drop-down-multilevel">
                    <li><a href="widget.html">Seminar</a></li>
                    <li><a href="event-calendar.html">Cek Sertifikat</a></li>
                    <li><a href="event-calendar.html">Arsip Kegiatan</a></li>
                    <li><a href="event-calendar.html">Dokumentasi</a></li>
                </ul>
            </li> -->
                                    <!-- <li><a href="#berita">Berita</i></a></li> -->
                                </ul>
                            </div>
                        </section>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- menu end -->
</header>