@extends('website.layout.app')
@section('content')
    <div class="container ">
        <div class="row pt120">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="heading align-left">
                    <div class="h2 heading-title text-bold-800 mb-0">Mitra Kerja Sama</div>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">Kami telah menjalin kerja sama berbagai bidang instansi <br> pendidikan, industri, serta pemerintahan.
                    </p>
                </div>
            </div>
        </div>

        <div class="row mb-5">
            @foreach($partner as $item)
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="client-item-style2 col-3 bg-border-color mb10 pb-1">
                        <div class="client-image mb-2">
                            <img src="{{ $item->getFirstMediaUrl()  }}" alt="logo">
                            <img src="{{ $item->getFirstMediaUrl()  }}" alt="logo" class="hover">
                        </div>
                        <div class="text-capitalize font-medium-1 text-bold-700 c-dark" style="height: 80px">{!! $item->name !!}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
