@extends('website.layout.app')
@section('content')
<sectio class="page-section-ptb position-relative">

    <section class="our-team white-bg page-section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 sm-mb-30">
                    <img class="img-fluid full-width" src="{{ $pengurus->getFirstMediaUrl() }}" alt="">
                </div>
                <div class="col-lg-8">
                    <div class="team-details">
                        <div class="clearfix">
                            <div class="title float-left mb-20">
                                <h2 class="theme-color mb-3">Biodata</h2>
                                <p>
                                <p> Nama : {{ $pengurus->name_full }}</p>
                                <p> Masa Berlaku : {{ $pengurus->masa_berlaku}}</p>
                                <p> Email : {{ $pengurus->email_valid}}</p>
                                <p> Alamat : {{$pengurus->address }}</p>
                                <p> Asal Kampus : {{ $pengurus->perguruan_tinggi}}</p>
                                <p> Asal Prodi : {{$pengurus->program_studi}}</p>
                                </p>
                                <h4 class="mb-20 theme-color">Sosial Media</h4>
                                 <ul class="list list-unstyled mb-30">
                                    @foreach($pengurus->sosmed as $item)
                                   <li><i class="fa fa-{{$item->key}}"></i> {{$item->value}} </li>
                                   @endforeach
                                 </ul>
                                 <h4 class="mb-20 theme-color">Riwayat Pendidikan</h4>
                                 <ul class="list list-unstyled mb-30">
                                    @foreach($pengurus->personHasPendidikans as $item)
                                   <li><i class="fa fa-angle-right"></i> {{$item->perguruan_tinggi.'-'. $item->program_studi.'('.$item->pendidikan->name.')'. $item->tahun_lulus }} </li>
                                   @endforeach
                                 </ul>
                                 <h4 class="mb-20 theme-color">Portofolio</h4>
                                 <ul class="list list-unstyled mb-30">
                                    @foreach($pengurus->personPorto as $item)
                                   <li><i class="fa fa-angle-right"></i> {{$item->name}} </li>
                                   @endforeach
                                 </ul>
                                 <h4 class="mb-20 theme-color">Kemampuan</h4>
                                 <ul class="list list-unstyled mb-30">
                                    @foreach($pengurus->personSkill as $item)
                                   <li><i class="fa fa-angle-right"></i> {{$item['skill']->name}} </li>
                                   @endforeach
                                 </ul>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    @endsection
    @section('css')
    <link type="text/css" rel="stylesheet" href="{!! asset('css/ycp.css') !!}" />

    <div id="fb-root"></div>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v10.0&appId=298637494983635&autoLogAppEvents=1"
        nonce="hYx81EtD"></script>

    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    @endsection
    @section('scripts')
    <!-- jQuery -->
    <script src="{!! asset('js/ycp.js') !!}"></script>
    <script>
    $(function() {

        $("#channel-dprd-kaltim").ycp({
            apikey: 'AIzaSyBy_wcF4Lnkd9uigQCuJaRNUNtgQ6RIXdI',
            playlist: 7,
            autoplay: false,
            related: true
        });

    });
    </script>
    @endsection