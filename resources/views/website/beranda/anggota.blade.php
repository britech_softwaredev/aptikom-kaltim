@extends('website.layout.app')
@section('content')
<sectio class="page-section-ptb position-relative">

    <section id="team" class="page-section-ptb">
        <div class="container">
            <div class="row">
            <div class="section-title col-md-12 text-center mb-40">
                    <h2 class="title-effect ">Struktur Kepengurusan Aptikom Kaltim</h2>
                </div>
                
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-6 sm-mb-30">
                    <div class="team team-hover team-border">
                        <div class="mb-3 text center">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                    src="{{asset('image/ketua.png') }}" alt="logo"> </a>
                        </div>
                        <div class="team-description">
                            <div class="team-info">
                                <h5><a href="team-single.html"> Eko Junirianto </a></h5>
                                <span>Ketua Aptikom Kaltim</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    <div class="row">
                    <div class="col-md-12">
                    </div>
                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 xs-mb-30">
                        <div class="team team-hover team-border">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/yuni.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Sri Wahyuni </a></h5>
                                    <span>Sekertaris Aptikom Kaltim</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="section-title line-dabble">
                <h4 class="title">Bidang I</h4>
            </div>
            <div class="container">
                <div class="row">
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="mb-3 text center">
                                <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/ariyadi.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Ariyadi </a></h5>
                                    <span>Ketua Bidang I</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    </div>
                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                                <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/cahyo.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Muhammad chandra cahyo utomo </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/yoga.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Tagfirul Azhima Yoga </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 xs-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/tina.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Tina Tri Wulansari </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/haris.jpeg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Sayekti Harist Suryawan </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-title line-dabble">
                <h4 class="title">Bidang II</h4>
            </div>
            <div class="container">
                <div class="row">
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="mb-3 text center">
                                <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/ariyadi.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Ariyadi </a></h5>
                                    <span>Ketua Bidang II</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    </div>

                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                                <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/cahyo.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Muhammad chandra cahyo utomo </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/yoga.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Tagfirul Azhima Yoga </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 xs-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/tina.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Tina Tri Wulansari </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/haris.jpeg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Sayekti Harist Suryawan </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-title line-dabble">
                <h4 class="title">Bidang III</h4>
            </div>
            <div class="container">
                <div class="row">
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="mb-3 text center">
                                <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/ariyadi.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Ariyadi </a></h5>
                                    <span>Ketua Bidang III</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    </div>

                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                                <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/cahyo.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Muhammad chandra cahyo utomo </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/yoga.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Tagfirul Azhima Yoga </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 xs-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/tina.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Tina Tri Wulansari </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/haris.jpeg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Sayekti Harist Suryawan </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-title line-dabble">
                <h4 class="title">Bidang IV</h4>
            </div>
            <div class="container">
                <div class="row">
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="mb-3 text center">
                                <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/ariyadi.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Ariyadi </a></h5>
                                    <span>Ketua Bidang IV</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    </div>

                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                                <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/cahyo.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Muhammad chandra cahyo utomo </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 sm-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/yoga.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Tagfirul Azhima Yoga </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 xs-mb-30">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/tina.jpg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Tina Tri Wulansari </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="team team-hover team-border">
                            <div class="team-photo">
                            <a href="index-01.html"><img style="width: 150px;" id="logo_img"
                                        src="{{asset('image/haris.jpeg')}}" alt="logo"> </a>
                            </div>
                            <div class="team-description">
                                <div class="team-info">
                                    <h5><a href="team-single.html"> Sayekti Harist Suryawan </a></h5>
                                    <span>Anggota</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </section>



    @endsection
    @section('css')
    <link type="text/css" rel="stylesheet" href="{!! asset('css/ycp.css') !!}" />

    <div id="fb-root"></div>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v10.0&appId=298637494983635&autoLogAppEvents=1"
        nonce="hYx81EtD"></script>

    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    @endsection
    @section('scripts')
    <!-- jQuery -->
    <script src="{!! asset('js/ycp.js') !!}"></script>
    <script>
    $(function() {

        $("#channel-dprd-kaltim").ycp({
            apikey: 'AIzaSyBy_wcF4Lnkd9uigQCuJaRNUNtgQ6RIXdI',
            playlist: 7,
            autoplay: false,
            related: true
        });

    });
    </script>
    @endsection