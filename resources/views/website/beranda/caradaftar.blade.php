@extends('website.layout.app')
@section('content')


<section class="gray-bg blog masnary-blog-3-columns masonry-main page-section-ptb">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section-title text-center">
                    <h2>Cara <span class="theme-color"> Pendaftaran</span> </h2>
                    <img src="https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.10-AM-1024x491.jpeg"
                        alt="" class="mb-3"
                        srcset="https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.10-AM-1024x491.jpeg 1024w, https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.10-AM-300x144.jpeg 300w, https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.10-AM-768x368.jpeg 768w, https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.10-AM.jpeg 1280w"
                        sizes="(max-width: 1024px) 100vw, 1024px">
                    <img src="https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.20-AM-1024x491.jpeg"
                        alt="" class="mt-3"
                        srcset="https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.20-AM-1024x491.jpeg 1024w, https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.20-AM-300x144.jpeg 300w, https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.20-AM-768x368.jpeg 768w, https://www.aptikomjabar.org/wp-content/uploads/2019/02/WhatsApp-Image-2019-01-22-at-9.45.20-AM.jpeg 1280w"
                        sizes="(max-width: 1024px) 100vw, 1024px">
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('css')
<link type="text/css" rel="stylesheet" href="{!! asset('css/ycp.css') !!}" />

<div id="fb-root"></div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v10.0&appId=298637494983635&autoLogAppEvents=1"
    nonce="hYx81EtD"></script>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endsection
@section('scripts')
<!-- jQuery -->
<script src="{!! asset('js/ycp.js') !!}"></script>
<script>
$(function() {

    $("#channel-dprd-kaltim").ycp({
        apikey: 'AIzaSyBy_wcF4Lnkd9uigQCuJaRNUNtgQ6RIXdI',
        playlist: 7,
        autoplay: false,
        related: true
    });

});
</script>
@endsection