@extends('website.layout.app')
@section('content')
<sectio class="page-section-ptb position-relative">
<section class="gray-bg blog masnary-blog-3-columns masonry-main page-section-ptb">
   <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-8">
       <div class="section-title text-center">
        <h2>Data Anggota <span class="theme-color"> Aptikom kaltim</span> </h2>
       </div>
      </div>
    </div>
   </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="row">
          <div class="col-3">
          <div class="card p-0">
        {!! Form::open(['method' => 'get'], ['class' => 'form']) !!}
          <div class="input-group shadow-sm">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"><button type="submit"><i class="fa fa-search"></i></button></span>
            </div>
            <input type="text" name="name_full" class="form-control" placeholder="Cari nama anggota" aria-label="Username" aria-describedby="basic-addon1">
          </div>
        {{ Form::close() }}
        </div>
          </div>
        </div>
      </div>
    @foreach($pengurus as $item) 
     <div class="col-2">
       <!-- <div class="owl-carousel" data-nav-dots="true" data-items="5" data-md-items="4" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="20"> -->
       <div class="item">
         <div class="blog-overlay">
         <div class="blog-image">
         <div style="background-image:url('{{ $item->getFirstMediaUrl() }}') ; background-size:cover; background-repeat:no-repeat;" class="height-250"></div>
         </div>
         <div class="blog-name">
            <h4 class="mt-15 text-white"><a href="{{route('detailPengurus',[$item->slug])}}">{{ $item->name_full }}</a></h4>
        </div>
      </div>
     </div>
     
     <!-- </div> -->
    </div>
    @endforeach
   </div>
  </div>
 </section>
@endsection
@section('css')
<link type="text/css" rel="stylesheet" href="{!! asset('css/ycp.css') !!}" />

<div id="fb-root"></div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v10.0&appId=298637494983635&autoLogAppEvents=1"
    nonce="hYx81EtD"></script>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endsection
@section('scripts')
<!-- jQuery -->
<script src="{!! asset('js/ycp.js') !!}"></script>
<script>
$(function() {

    $("#channel-dprd-kaltim").ycp({
        apikey: 'AIzaSyBy_wcF4Lnkd9uigQCuJaRNUNtgQ6RIXdI',
        playlist: 7,
        autoplay: false,
        related: true
    });

});
</script>
@endsection