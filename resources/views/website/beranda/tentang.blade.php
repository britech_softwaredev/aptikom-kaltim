@extends('website.layout.app')
@section('content')
<sectio class="page-section-ptb position-relative">
    <section class="gray-bg blog masnary-blog-3-columns masonry-main page-section-ptb">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title text-center">
                        <h2 class="title">Sejarah Aptikom Kaltim </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="masonry columns-1">
                        <!-- ======================================= -->
                        <div class="rounded-2 masonry-item">
                            <div class="blog-entry blockquote mb-0 bg-white">
                                <div class="entry-blockquote clearfix">
                                    <div class="mt-60 blockquote">
                                        <div class="mb-3 text-center">
                                            <a href="index-01.html"><img style="width: 300px;" id="logo_img" src="{{asset('image/logo.png') }}" alt="logo"> </a>
                                        </div>
                                    
                                        <p>Semakin banyak dan berkembangnya sekolah dan perguruan tinggi di bidang
                                            Information Technology (IT), menyebabkan peningkatan persaingan atara
                                            lembaga-lembaga pendidikan tersebut untuk menghasilkan lulusan yang
                                            berkualitas.
                                            Atmosfer persaingan yang sehat dibutuhkan untuk memacu perkembangan IT di
                                            Indonesia.</p>

                                        <p>Semakin banyak dan berkembangnya sekolah dan perguruan tinggi di bidang
                                            Information Technology (IT), menyebabkan peningkatan persaingan atara
                                            lembaga-lembaga pendidikan tersebut untuk menghasilkan lulusan yang
                                            berkualitas.
                                            Atmosfer persaingan yang sehat dibutuhkan untuk memacu perkembangan IT di
                                            Indonesia.</p>
                                            

                                        <p>Saat ini tercatat lebih dari 500.000 (lima ratus ribu) mahasiswa D1 hingga S3
                                            yang sedang aktif belajar di lebih dari 850 (delapan ratus lima puluh)
                                            Perguruan
                                            Tinggi di Indonesia di bawah naungan sekitar 1.500 program studi Kampus
                                            Informatika dan Komputer di seluruh Indonesia, dengan jumlah lulusan sekitar
                                            40.000 (empat puluh ribu) hingga 50.000 (lima puluh ribu) alumni per
                                            tahunnya.
                                            APTIKOM sebagai sebuah asosiasi yang merupakan kerjasama antara perguruan
                                            tinggi
                                            di bidang IT diharapkan dapat menjadi wadah untuk dapat meningkatkan
                                            kerjasama
                                            baik antar perguruan tinggi, maupun antar perguruan tinggi dengan pemerintah
                                            dan
                                            pelaku usaha.</p>

                                        <p>Semuanya bermula pada tahun 1983, yaitu ketika 8 (delapan) perguruan tinggi
                                            penggagas pendirian Program Studi informatika dan komputer membentuk sebuah
                                            forum yang diberi nama Badan Kerja Sama Perguruan Tinggi Sejenis Ilmu
                                            Komputer
                                            (BKS PERTINIS I-K).
                                            Sejalan dengan perkembangan pesat ilmu komputer dan informatika, anggota BKS
                                            PERTINIS I-K bertambah menjadi 78 (Tujuh Puluh Delapan) anggota pada tahun
                                            1987.
                                            Puncaknya adalah pada tahun 1996, ketika BKS PERTINIS I-K berubah nama
                                            menjadi
                                            Perguruan Tinggi Informatika dan Komputer (PERTIKOM) jumlah anggota menjadi
                                            lebih dari 250 Program Studi dari seluruh wilayah tanah air.

                                        <p>Sesuai dengan kebutuhan dan perkembangan jaman, maka pada Rapat Koordinasi
                                            Nasional (Rakornas) 2002 di kota Malang, diusulkanlah perubahan bentuk
                                            organisasi dari PERTIKOM menjadi (Asosiasi Perguruan Tinggi Informatika dan
                                            Ilmu
                                            Komputer (APTIKOM). Sampai dengan saat ini, jumlah anggota APTIKOM terdiri
                                            dari
                                            850 (Delapan Ratus Lima Puluh) Kampus Informatika dan Komputer serta 1560
                                            (Seribu Lima Ratus Enam Puluh ) Program Studi.</p>

                                        <p>APTIKOM Provinsi Jawa Barat merupakan perluasan jangkauan Wilayah. Sebelumnya
                                            adalah APTIKOM Wilayah 4 yang meliputi Jawa barat dan Banten, yang di Pimpin
                                            oleh Prof. Iping Supriana. Pada tahun 2013 di Gelar Musyawarah Provinsi
                                            pertama
                                            untuk memilih ketua Provinsi sebagai Perluasan dari APTIKOM Wilayah 4 dan
                                            terpilih Prof. Dr. Ir. Eddy Jusuf, Sp., M.Si., M. Kom. (Rektor Universitas
                                            Pasundan) sebagai Ketua Provinsi.</p>
                                        <cite class="text-black"> – Aptikom kaltim</cite>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="event" class="page-section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title line left">
                        <h2 class="title">Visi </h2>
                        <p>Pada tahun 2022 menjadi asosiasi terkemuka yang bereputasi internasional, memiliki jenjang
                            global, dan memberikan kontribusi signifikan bagi peningkatan kualitas Pendidikan Tinggi
                            Informatika dan Komputer di Indonesia</p>
                    </div>
                </div>
                <div class="col-md-12 xs-mt-40">
                    <div class="section-title line left">
                        <h2 class="title">Misi </h2>
                    </div>
                    <div class="owl-carousel" data-nav-dots="true" data-items="1" data-lg-items="1" data-md-items="1"
                        data-sm-items="1" data-xs-items="1">
                        <div class="item">
                            <div class="testimonial bottom_pos">
                                <div class="testimonial-avatar"> <img alt=""
                                        src="demo-one-page/hotel/images/team/01.jpg"> </div>
                                <div class="testimonial-info">
                                    <h6> Membina dan mengembangkan kapasitas Perguruan Tinggi pada
                                        umumnya dan Program Studi bidang Informatika dan Komputer pada khususnya </h6>
                                </div>
                                <div class="author-info">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial bottom_pos">
                                <div class="testimonial-avatar"> <img alt=""
                                        src="demo-one-page/hotel/images/team/02.jpg"> </div>
                                <div class="testimonial-info">
                                    <h6> Meningkatkan mutu pendidikan, penelitian dan pengembangan
                                        di bidang Informatika dan Komputer</h6>
                                </div>
                                <div class="author-info">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial bottom_pos">
                                <div class="testimonial-avatar"> <img alt=""
                                        src="demo-one-page/hotel/images/team/02.jpg"> </div>
                                <div class="testimonial-info">
                                    <h6> Membantu pemerintah dalam rangka pengembangan standar-standar terkait
                                        penyelenggaraan pendidikan tinggi di bidang Informatika dan Komputer </h6>
                                </div>
                                <div class="author-info">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial bottom_pos">
                                <div class="testimonial-avatar"> <img alt=""
                                        src="demo-one-page/hotel/images/team/03.jpg"> </div>
                                <div class="testimonial-info">
                                    <h6> Merintis dan melakukan upaya kerjasama dengan berbagai pihak, pemerintah,
                                        industri, asosiasi lain, lembaga-lembaga baik dalam maupun luar negeri dalam
                                        bidang Informatika dan Komputer </h6>
                                </div>
                                <div class="author-info">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-section-ptb gray-bg">
  <div class="container">
     <div class="row">
     <div class="col-lg-12 col-md-12">
         <div class="section-title text-center">
            <h2 class="title-effect">Arti Logo Aptikom</h2>
          </div>
       </div>
    </div>
    <div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="owl-carousel" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
          <div class="item"><div class="testimonial clean">
          <div class="testimonial-avatar"> <img alt="" src="{{asset('image/aptikom.jpg') }}"> </div>
          <div class="author-info"> <strong>Bola Dunia</strong> </div>
          <div class="testimonial-info"> Menggambarkan cara pandang ilmu yang mendunia dan mempersiapkan lulusannya untuk siap kompetisi global.</div>
          
      </div>
      </div>
      <div class="item">
       <div class="testimonial clean">
          <div class="testimonial-avatar"> <img alt="" src="{{asset('image/aptikom.jpg') }}"> </div>
          <div class="author-info"> <strong>Tiga Bujur Sangkar</strong> </div>
          <div class="testimonial-info"> Mempresentasikan "Tri Dharma Perguruan Tinggi", Mengembangkan tiga tipe kompetensi utama, dan Membangun tiga pusat kehidupan manusia.</div>
      </div>
      </div>
      <div class="item">
       <div class="testimonial clean">
          <div class="testimonial-avatar"> <img alt="" src="{{asset('image/aptikom.jpg') }}"> </div>
          <div class="author-info"> <strong>Warna Biru</strong> </div>
           <div class="testimonial-info"> Mempresentasikan lautan yang teduh, dimana sejalan dengan filosofi kerjasama saling menguntungkan (blue ocean strategy), koordinasi yang dinamis dan damai, serta kekuatan tersembunyi yang dahsyat.  </div>
        </div>
       </div>
       <div class="item">
       <div class="testimonial clean">
          <div class="testimonial-avatar"> <img alt="" src="{{asset('image/aptikom.jpg') }}"> </div>
          <div class="author-info"> <strong>Warna Biru</strong> </div>
           <div class="testimonial-info"> Mempresentasikan angkasa yang tak terbatas sifatnya, sejalan dengan proses penggantungan cita-cita, semangat belajar yang tiada henti, dan ilmu pengetahuan yang tak berkesudahan.  </div>
        </div>
       </div>
     </div>
    </div>
   </div>
 </div>
</section>
    @endsection
    @section('css')
    <link type="text/css" rel="stylesheet" href="{!! asset('css/ycp.css') !!}" />

    <div id="fb-root"></div>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v10.0&appId=298637494983635&autoLogAppEvents=1"
        nonce="hYx81EtD"></script>

    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    @endsection
    @section('scripts')
    <!-- jQuery -->
    <script src="{!! asset('js/ycp.js') !!}"></script>
    <script>
    $(function() {

        $("#channel-dprd-kaltim").ycp({
            apikey: 'AIzaSyBy_wcF4Lnkd9uigQCuJaRNUNtgQ6RIXdI',
            playlist: 7,
            autoplay: false,
            related: true
        });

    });
    </script>
    @endsection
