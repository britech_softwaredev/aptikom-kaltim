@extends('website.layout.app')
@section('content')

<section class="gray-bg blog masnary-blog-3-columns masonry-main page-section-ptb">
<div class="col-md-12">
    <div class="section-title line left">
        <h2 class="title">Pendaftaran </h2>
        <pre>Assalamu ‘alaikum wr.wb.

            Bpk/Ibu Pimpinan Institusi/Dekan/Ka.Prodi dan Dosen Bidang Informatika dan Komputer.

            Keanggotaan Aptikom terdiri dari 2 (dua) unsur, yaitu :
            1. Individu/Perorangan dan
            2. Institusi

            Prosedur pendaftaran INDIVIDU/PERORANGAN :
            1. Isi form pendaftaran silahkan pilih link google form yang di inginkan 
             a) Formulir Pendaftaran Bagi Keanggota Perorangan atau 
             b) Formulir Perpanjangan Anggota Perorangan (+unggah bukti trf).
            2. Tim Sekretariat akan mengirim Nomor Anggota dan pass login MYAPTIKOM via Email.
            3. Install MYAPTIKOM
            4. Daftar online, dengan entri nomor anggota dan passw login di MYAPTIKOM.
            5. Selesai

            Prosedur pendaftaran
            INSTITUSI :
            1. Isi form pendaftaran, silahkan pilih link google : 
             (a) Formulir Pendaftaran Baru Anggota Institusi atau
             (b) Formulir Perpanjangan Anggota Institusi (+unggah bukti trf).
            2. Tim Sekretariat akan mengirim Nomor Anggota dan passw login MYAPTIKOM via Email.
            3. Install MYAPTIKOM
            4. Daftar online, dengan entri nomor anggota dan passw login MYAPTIKOM.
            5. Selesai.

            Keterangan :
            Pengisian formulir yg Institusi baik utk pendaftaran baru ataupun perpanjangan dibagi menjadi 3 bagian (form
            akan muncul 3 tahap pengisian) yaitu :

            Bagian 1 : Isi email pengisi formulir
            Bagian 2 : Isi Data Pemegang Kuasa
            Bagian 3 : Isi Data institusi.

            Cara Install MYAPTIKOM :
            1. Klik Google Play Store
            2. Search : My Aptikom
            3. Klik My Aptikom
            4. Klik Install

            Biaya Iuran Keanggotaan APTIKOM per Tahun :
            1. Individu/Perorangan *(BARU) Rp. 200,000,- (Dua Ratus Ribu Rupiah).
            2. Individu/Perorangan *(PERPANJANGAN) Rp. 150,000,- (Seratus Lima Puluj Ribu Rupiah).
            3. Intitusi *(BARU) Rp. 1,000,000,- (Satu Juta Rupiah).
            4. Intitusi *(PERPANJANGAN) Rp. 750,000,- (Tujuh Ratus Lima Puluh Ribu Rupiah).

            Nomor Rek. APTIKOM : BNI 46 cabang Perintis Kemerdekaan 1119995552.

            Terdapat 4 link Google Form yaitu untuk :
            1. Pendaftaran Anggota Perorangan/Individu (BARU)
            <a href="https://goo.gl/9VYH5H">Link Pendaftaran Klik Disini</a>

            2. Pendaftaran Anggota Perorangan/Individu (PERPANJANGAN)
            <a href="https://goo.gl/bfxPPG">Link Pendaftaran Klik Disini</a>

            3. Pendaftaran Anggota Institusi (BARU)
            <a href="https://goo.gl/Qd91Zi">Link Pendaftaran Klik Disini</a>
            
            4. Pendaftaran Anggota Institusi (PERPANJANGAN)
            <a href="https://goo.gl/kbmZXX">Link Pendaftaran Klik Disini</a>
            </pre>
    </div>
</div>
</section>

@endsection
@section('css')
<link type="text/css" rel="stylesheet" href="{!! asset('css/ycp.css') !!}" />

<div id="fb-root"></div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v10.0&appId=298637494983635&autoLogAppEvents=1"
    nonce="hYx81EtD"></script>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endsection
@section('scripts')
<!-- jQuery -->
<script src="{!! asset('js/ycp.js') !!}"></script>
<script>
$(function() {

    $("#channel-dprd-kaltim").ycp({
        apikey: 'AIzaSyBy_wcF4Lnkd9uigQCuJaRNUNtgQ6RIXdI',
        playlist: 7,
        autoplay: false,
        related: true
    });

});
</script>
@endsection