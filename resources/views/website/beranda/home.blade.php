@extends('website.layout.app')
@section('content')

<section class="rev-slider">
    <div id="rev_slider_275_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
        data-alias="webster-slider-10" data-source="gallery"
        style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.4.6.3 fullwidth mode -->
        <div id="rev_slider_275_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.3">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-769" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                    data-thumb="" data-delay="7000" data-rotate="0" data-saveperformance="off" data-title="Slide"
                    data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                    data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="https://pcr.ac.id/assets/images/artikel/Tingkatkan_Kompetensi_Masyarakat_TIK_APTIKOM_Adakan_Seminar_Nasional_di_PCR20180728101354.jpg"
                        data-bgcolor='#141414' style='background:#141414' alt="" data-bgposition="center center"
                        data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg"
                        data-no-retina>
                    <!-- LAYERS -->
                </li>
                <li data-index="rs-770" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                    data-thumb="" data-delay="7000" data-rotate="0" data-saveperformance="off" data-title="Slide"
                    data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                    data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="https://i1.wp.com/www.wartaperawat.com/wp-content/uploads/2018/05/IMG-20180507-WA0022-1-edit.jpg?fit=640%2C362&ssl=1"
                        data-bgcolor='#141414' style='background:#141414' alt="" data-bgposition="center center"
                        data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg"
                        data-no-retina>
                    <!-- LAYERS -->
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="page-section-ptb white-bg o-hidden">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h2 class="title-effect ">Seputar Aptikom</h2>
                </div>
            </div>
            {{--            <div class="container">--}}
            {{--                <div class="isotope-filters">--}}
            {{--                    <button data-filter="" class="active">Berita</button>--}}
            {{--                    <button data-filter=".photography">Informasi Umum</button>--}}
            {{--                    <button data-filter=".illustration">Seminar</button>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title line-dabble">
                            <h4 class="title">Berita</h4>
                        </div>
                    </div>
                    @foreach($posts as $post)
                    <div class="col-md-4">
                        <div class="blog-overlay rounded-top">
                            <img class="img-fluid rounded-top"
                                src="{{ asset($post->getFirstMediaUrl('default','thumb')) }}"
                                style="height: 180px;width: 100%;object-fit: cover">
                        </div>
                        <div class="blog-entry box-shadow-0-1">
                            <div class="blog-detail p-3">
                                <h6 class="text-black text-bold-600 font-small-3 mb-10 desc-p"><a
                                        href="{{route('detail-post', $post->slug)}}">{{$post->title}}</a> </h6>
                                <div class="entry-content">
                                    <p class="desc-p x-desc">
                                        {!! strip_tags($post->content,'/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si') !!}
                                    </p>
                                </div>
                                <div class="entry-meta text-blue-grey">
                                    <ul>
                                        <li><span class="font-small-2 text-bold-600"><i
                                                    class="fa fa-calendar-o"></i>{{ date('d F Y', strtotime($post->created_at)) }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-3 sm-mt-30">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title line-dabble mb-10">
                            <h4 class="title">Facebook</h4>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-3 col-sm-12">
                        <div class="blog blog-simple text-left mb-20">
                            <div class="blog-image">
                                <div style="background-image:url('{{asset('image/facebook.jpeg')}}') ; background-size:cover; background-repeat:no-repeat;"
                                    class="height-300"></div>
                            </div>
                            <div class="blog-name mt-20">
                                <h6 class="mt-15"><a href="#">Facebook Aptikom</a></h6>
                                <div class="admin">
                                    <a href="https://www.facebook.com/aptikom.kaltim">Kunjungi -></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-title line-dabble mb-10">
                        <h4 class="title">Instagram</h4>
                    </div>
                    <div class="col-lg-12 col-md-3 col-sm-12">
                        <div class="blog blog-simple text-left mb-20">
                            <div class="blog-image">
                                <div style="background-image:url('{{asset('image/instagram.jpeg')}}') ; background-size:cover; background-repeat:no-repeat;"
                                    class="height-300"></div>
                            </div>
                            <div class="blog-name mt-15">
                                <h6 class="mt-15"><a href="#">Instagram Aptikom</a></h6>
                                <div class="admin">
                                    <a href="https://instagram.com/aptikompusat?igshid=YmMyMTA2M2Y=">Kunjungi -></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-title line-dabble mb-10">
                        <h4 class="title">Youtube</h4>
                    </div>
                    <div class="col-lg-12 col-md-3 col-sm-12">
                        <div class="blog blog-simple text-left mb-20">
                            <div class="blog-image">
                                <div style="background-image:url('{{asset('image/youtube.jpeg')}}') ; background-size:cover; background-repeat:no-repeat;"
                                    class="height-300"></div>
                            </div>
                            <div class="blog-name mt-20">
                                <h6 class="mt-10"><a href="#">Youtube Aptikom</a></h6>
                                <div class="admin">
                                    <a href="https://youtube.com/channel/UCGREVutOnGA1VaLcIawFlBA">Kunjungi -></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gray-bg page-section-pt happy-clients">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 align-self-end">
                <img class="d-xs-block d-lg-block d-none img-fluid" src="{{asset('image/ketua.png') }}" alt="">
            </div>
            <div class="col-lg-6 mt-60">
                <div class="section-title">
                    <h2 class="title-effect">Sambutan Ketua Aptikom Kaltim</h2>
                </div>
                <div class="tab">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="testi-01">
                            <span class="quoter-icon">“</span>
                            <p>Asosiasi Pendidikan Tinggi Informatika dan Komputer (APTIKOM) Provinsi Kalimantan Timur
                                adalah salah satu bagian dari Asosiasi atau Organisasi yang ada di Provinsi Kalimantan
                                Timur. Asosiasi Pendidikan Tinggi Informatika dan Komputer (APTIKOM) memiliki tugas
                                secara langsung berkaitan dengan menghimpun dan mempersatukan institusi penyelenggara
                                pendidikan bidang informatika dan komputer.</p>
                            <h5>Eko Junirianto S.kom M.Cs </h5>
                            <span>Ketua Aptikom Kaltim</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="custom-content-03 page-section-ptb pattern full-width"
    style="background: url({{asset('image/background2.jpg') }}) repeat;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h4 class=" text-white">APTIKOM Provinsi Kalimantan Timur </h4>
                <h2 class=" text-white">Galeri Foto</h2>
            </div>
        </div>
        <div class="isotope popup-gallery columns-4 mt-40">
            <div class="grid-item photography branding">
                <div class="portfolio-item">
                    <div style="background-image:url('{{asset('https://i0.wp.com/universitasbumigora.ac.id/wp-content/uploads/2017/10/IMG_9947.jpg?fit=800%2C533&ssl=1')}}') ; background-size:cover; background-repeat:no-repeat;"
                        class="height-200"></div>

                    <div class="portfolio-overlay">
                        <h4 class="text-white"> Rakornas 2016 </a> </h4>
                    </div>
                    <a class="popup portfolio-img"
                        href="{{asset('https://i0.wp.com/universitasbumigora.ac.id/wp-content/uploads/2017/10/IMG_9947.jpg?fit=800%2C533&ssl=1') }}"><i
                            class="fa fa-arrows-alt"></i></a>
                </div>
            </div>
            <div class="grid-item photography branding">
                <div class="portfolio-item">
                    <div style="background-image:url('{{asset('http://aptikom-kalsel.or.id/theme/images/slider-3.jpg')}}') ; background-size:cover; background-repeat:no-repeat;"
                        class="height-200"></div>

                    <div class="portfolio-overlay">
                        <h4 class="text-white">Bimbingan Teknis APTIKOM </a> </h4>

                    </div>
                    <a class="popup portfolio-img"
                        href="{{asset('http://aptikom-kalsel.or.id/theme/images/slider-3.jpg') }}"><i
                            class="fa fa-arrows-alt"></i></a>
                </div>
            </div>
            <div class="grid-item photography branding">
                <div class="portfolio-item">
                    <div style="background-image:url('{{asset('http://aptikom-kalsel.or.id/assets/images/7fd4446c85a2d687909cdc6cab547980.jpg')}}') ; background-size:cover; background-repeat:no-repeat;"
                        class="height-200"></div>
                    <div class="portfolio-overlay">
                        <h4 class="text-white"> Rapat Kordinasi APTIKOM </a> </h4>
                    </div>
                    <a class="popup portfolio-img"
                        href="{{asset('http://aptikom-kalsel.or.id/assets/images/7fd4446c85a2d687909cdc6cab547980.jpg') }}"><i
                            class="fa fa-arrows-alt"></i></a>
                </div>
            </div>
            <div class="grid-item photography branding">
                <div class="portfolio-item">
                    <div style="background-image:url('{{asset('https://informatika.unimal.ac.id/dah_image/post/1036386131.jpg')}}') ; background-size:cover; background-repeat:no-repeat;"
                        class="height-200"></div>
                    <div class="portfolio-overlay">
                        <h4 class="text-white"> Musyawarah Provinsi Ke-2 </a> </h4>
                    </div>
                    <a class="popup portfolio-img"
                        href="{{asset('https://informatika.unimal.ac.id/dah_image/post/1036386131.jpg') }}"><i
                            class="fa fa-arrows-alt"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
@section('css')
<link type="text/css" rel="stylesheet" href="{!! asset('css/ycp.css') !!}" />

<div id="fb-root"></div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v10.0&appId=298637494983635&autoLogAppEvents=1"
    nonce="hYx81EtD"></script>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endsection
@section('scripts')
<!-- jQuery -->
<script src="{!! asset('js/ycp.js') !!}"></script>
<script>
$(function() {

    $("#channel-dprd-kaltim").ycp({
        apikey: 'AIzaSyBy_wcF4Lnkd9uigQCuJaRNUNtgQ6RIXdI',
        playlist: 7,
        autoplay: false,
        related: true
    });

});
</script>
@endsection