@extends('website.layout.app')
@section('content')
{{--<section id="team" class="page-section-ptb">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="section-title col-md-12 text-center mb-40">--}}
{{--                <h2 class="title-effect ">Galeri Foto</h2>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-md-4">--}}
{{--            <div class="blog-overlay rounded-top">--}}
{{--                <img class="img-fluid rounded-top" src="{{ asset($galeri->getFirstMediaUrl('default','thumb')) }}"--}}
{{--                    style="height: 180px;width: 100%;object-fit: cover">--}}
{{--            </div>--}}
{{--            <div class="entry-meta text-blue-grey">--}}
{{--                <ul>--}}
{{--                    <li><span class="font-small-2 text-bold-600"><i--}}
{{--                                class="fa fa-calendar-o"></i>{{ date('d F Y', strtotime($galeri->created_at)) }}</span>--}}
{{--                    </li>--}}
{{--                </ul>--}}

{{--            </div>--}}
{{--        </div>--}}

<style>
    p {
        color: black;
        font-weight: 500;
    }
</style>
<section class="blog blog-single white-bg pt-40 mb-40 mt-50">
    <div class="container">
        <div class="mb-30 mt-30">
            <div class="text-center">
{{--                <div class="divider double"></div>--}}
                <h1 class="title text-bold-700 mb-0 p-2">{!! $galeri->title !!}</h1>
                <p>{!! $galeri->content !!}</p>
                <div class="divider double"></div>
            </div>
        </div>
        <div class="isotope popup-gallery no-title justify-content-center pb-20">
            @foreach($galeri->getMedia() as $media)
                <div class="grid-item p-2" style="width: 250px">
                    <div class="portfolio-item rounded">
                        <img src="{{ asset($media->getUrl('thumb')) }}" alt="">
                        <a class="popup portfolio-img" href="{{ asset($media->getUrl('cover')) }}"><i class="fa fa-arrows-alt"></i></a>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
</section>
@endsection
