@extends('website.layout.app')
@section('content')

<sectio class="page-section-ptb position-relative">

    <section id="team" class="page-section-ptb">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title col-md-12 text-center mb-40">
                        <h2 class="title-effect ">Galeri Foto</h2>
                    </div>
                </div>

                @foreach($listGaleri as $galeri)
                <div class="col-md-4">
                    <div class="blog-overlay rounded-top">
                        <img class="img-fluid rounded-top"
                            src="{{ asset($galeri->getFirstMediaUrl('default','thumb')) }}"
                            style="height: 180px;width: 100%;object-fit: cover">
                    </div>
                    <div class="blog-entry box-shadow-0-1">
                        <div class="blog-detail p-3">
                            <h6 class="text-black text-bold-600 font-small-3 mb-10 desc-p"><a
                                    href="{{route('detail-galeri-foto', $galeri->slug)}}">{{$galeri->title}}</a> </h6>
                            <div class="entry-meta text-blue-grey">
                                <ul>
                                    <li><span class="font-small-2 text-bold-600"><i
                                                class="fa fa-calendar-o"></i>{{ date('d F Y', strtotime($galeri->created_at)) }}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
</sectio>
@endsection