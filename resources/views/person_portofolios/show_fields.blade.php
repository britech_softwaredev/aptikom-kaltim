<!-- Sudah di modifikasi -->
<!-- Name Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('name', 'Name:') !!}
        </h5>
        {!! $personPortofolio->name !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Year Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('year', 'Year:') !!}
        </h5>
        {!! $personPortofolio->year !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Company Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('company', 'Company:') !!}
        </h5>
        {!! $personPortofolio->company !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Link Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('link', 'Link:') !!}
        </h5>
        {!! $personPortofolio->link !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Person Id Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('person_id', 'Person Id:') !!}
        </h5>
        {!! $personPortofolio->person_id !!}
    </div>
</div>

