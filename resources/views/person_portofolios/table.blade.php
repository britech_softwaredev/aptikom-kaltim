<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default table-responsive">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Person</th>
        <th>Name</th>
        <th>Year</th>
        <th>Company</th>
        <th>Link</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($personPortofolios as $personPortofolio)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $personPortofolio['person']['name'] !!}</td>
            <td>{!! $personPortofolio->name !!}</td>
            <td>{!! $personPortofolio->year !!}</td>
            <td>{!! $personPortofolio->company !!}</td>
            <td>{!! $personPortofolio->link !!}</td>
            <td>
                {!! Form::open(['route' => ['personPortofolios.destroy', $personPortofolio->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('personPortofolios.show', [$personPortofolio->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('personPortofolios.edit', [$personPortofolio->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
