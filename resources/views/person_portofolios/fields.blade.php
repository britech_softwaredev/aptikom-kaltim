{{--<!-- Person Id Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('person_id', 'Person') !!}--}}
{{--    <div class="position-relative">--}}
{{--        {!! Form::select('person_id', $person,null, ['class' => 'form-control']) !!}--}}
{{--    </div>--}}
{{--</div>--}}
<input type="text" id="person_id" name="person_id" value="{{ $person_id ?? $personPortofolio->person_id }}" hidden>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nama Portofolio') !!}
    <div class="position-relative">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Year Field -->
<div class="form-group">
    {!! Form::label('year', 'Year') !!}
    <div class="position-relative">
        {!! Form::text('year', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Company Field -->
<div class="form-group">
    {!! Form::label('company', 'Company') !!}
    <div class="position-relative">
        {!! Form::text('company', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link Projek') !!}
    <div class="position-relative">
        {!! Form::text('link', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! url()->previous() !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

