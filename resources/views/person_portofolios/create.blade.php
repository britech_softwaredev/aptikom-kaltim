@extends('layouts.app')

@section('content')
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-md-12">
                    <div class="card overflow-hidden">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="bg-green p-2 media-middle">
                                    <i class="fa fa-pencil-square font-large-2 text-white"></i>
                                </div>
                                <div class="media-body p-1">
                                    <span class="green font-medium-5">Input Person Portofolio</span><br>
                                    <span style="margin-top: -5px">Membuat Person Portofolio Baru</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @include('adminlte-templates::common.errors')
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                {!! Form::open(['route' => 'personPortofolios.store'], ['class' => 'form']) !!}
                                <div class="form-body">
                                    @include('person_portofolios.fields')
                                </div>
                                {!! Form::close() !!}

                                <table class="table table-striped mt-3">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Tahun</th>
                                        <th>Perusahaan</th>
                                        <th>Link</th>
                                        <th>#</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $index => $item)
                                        <tr>
                                            <td>{{ $index+1 }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->years }}</td>
                                            <td>{{ $item->company }}</td>
                                            <td><a href="{{ $item->link }}">{{ $item->link }}</a></td>
                                            <td>
                                                {!! Form::open(['route' => ['personPortofolios.destroy', $item->id], 'method' => 'delete']) !!}
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="{{ url('personPortofolios/'.$item->id.'/edit') }}" class="btn btn-success btn-sm"><i class="fa fa-pencil-square"></i></a>
                                                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                </div>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
