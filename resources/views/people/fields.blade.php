<div class="row">
    <div class="col-lg-6">
        <p class="text-bold-700 black font-medium-5">Biodata Anggota</p>
        <div class="form-group">
            {!! Form::label('foto', 'Foto :') !!}
            <div class="position-relative">
                @if(!isset($person))
                    <x-media-library-attachment name="media" rules="mimes:png,jpeg"/>
                @else
                    <x-media-library-collection :model="$person" max-items="1"  name="media" rules="mimes:png,jpeg"/>
                @endif
            </div>
        </div>
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('no_anggota', 'No Anggota:') !!}
            <div class="position-relative">
                {!! Form::text('no_anggota', null, ['class' => 'form-control']) !!}
            </div>
            <p class="text-bold-700 black">Last No Anggota : <span class="danger">{{ \App\Models\Person::orderBy('no_anggota','desc')->latest()->first()->no_anggota ?? '' }}</span></p>
        </div>
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('name_full', 'Nama:') !!}
            <div class="position-relative">
                {!! Form::text('name_full', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <!-- Date Of Birth Field -->
        <div class="form-group">
            {!! Form::label('date_of_birth', 'Tanggal Lahir:') !!}
            {!! Form::date('date_of_birth', isset($person) ? date('Y-m-d',strtotime($person->date_of_birth)) : '', ['class' => 'form-control']) !!}

        </div>
        <!-- Place Of Birth Field -->
        <div class="form-group">
            {!! Form::label('place_of_birth', 'Tempat Lahir:') !!}
            <div class="position-relative">
                {!! Form::text('place_of_birth', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <!-- Address Field -->
        <div class="form-group">
            {!! Form::label('address', 'Alamat:') !!}
            <div class="position-relative">
                {!! Form::text('address', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <!-- Address Field -->
        <div class="form-group">
            {!! Form::label('email_valid', 'Email:') !!}
            <div class="position-relative">
                {!! Form::text('email_valid', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <!-- Address Field -->
        <div class="form-group">
            {!! Form::label('no_hp', 'No Hp:') !!}
            <div class="position-relative">
                {!! Form::number('no_hp', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <!-- Address Field -->
        <div class="form-group">
            {!! Form::label('masa_berlaku', 'Masa Berlaku:') !!}
            {!! Form::date('masa_berlaku', isset($person) ? date('Y-m-d',strtotime($person->masa_berlaku)) : '', ['class' => 'form-control']) !!}

        </div>
        <!-- Description Field -->
        <div class="form-group">
            {!! Form::label('address', 'Alamat:') !!}
            <div class="position-relative">
                {!! Form::textarea('address', null, ['class' => 'form-control','rows'=>2]) !!}
            </div>
        </div>
        <!-- Order Field -->
        <div class="form-group">
            {!! Form::label('order', 'Order:') !!}
            <div class="position-relative">
                {!! Form::number('order', isset($person)?$person->order:0, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <p class="text-bold-700 black font-medium-5">Asal Institusi Pendidikan</p>
        {!! Form::label('perguruan_tinggi', 'Nama Perguruan Tinggi:') !!}
        <div class="position-relative mb-1">
            {!! Form::text('perguruan_tinggi',null, ['class' => 'form-control']) !!}
        </div>

        {!! Form::label('program_studi', 'Program Studi:') !!}
        <div class="position-relative mb-1">
            {!! Form::text('program_studi',null, ['class' => 'form-control']) !!}
        </div>

        {!! Form::label('alamat_perguruan_tinggi', 'Alamat Perguruan Tinggi:') !!}
        <div class="position-relative mb-1">
            {!! Form::text('alamat_perguruan_tinggi',null, ['class' => 'form-control']) !!}
        </div>

        {!! Form::label('jabatan_perguruan_tinggi', 'Jabatan Perguruan Tinggi:') !!}
        <div class="position-relative mb-1">
            {!! Form::text('jabatan_perguruan_tinggi',null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! route('people.index') !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/media-pro/styles.css')}}">

    <livewire:styles />
    <livewire:scripts />

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.min.js" defer></script>
@endsection
