<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th>No.</th>
        <th>No Anggota</th>
        <th>Nama</th>
{{--        <th>Tempat Lahir</th>--}}
{{--        <th>Tanggal Lahir</th>--}}
        <th>Address</th>
        <th>Foto</th>
{{--        <th>Jabatan</th>--}}
{{--        <th>Order</th>--}}
{{--        <th>Detail</th>--}}
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($people as $person)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $person->no_anggota !!}</td>
            <td>{!! $person->name_full !!}</td>
{{--            <td>{!! $person->place_of_birth !!}</td>--}}
{{--            <td>{!! isset($person->date_of_birth)?$person->date_of_birth->format('d/m/Y'):"" !!}</td>--}}
            <td>{!! $person->address !!}</td>
            <td>
                <img src="{{ $person->getFirstMediaUrl('default','thumb') }}" alt="avatar" class="height-70">
            </td>
{{--            <td>{!! $person->jabatan->name ?? '' !!}</td>--}}
{{--            <td>{!! $person->order !!}</td>--}}
{{--            <td>--}}
{{--                <a class="btn btn-sm btn-success mb-1" href="{!! url('personSkills/create?person_id='.$person->id) !!}">Skill +</a>--}}
{{--                <a class="btn btn-sm btn-success mb-1" href="{!! url('personPortofolios/create?person_id='.$person->id) !!}">Portofolio +</a>--}}
{{--                <a class="btn btn-sm btn-success mb-1" href="{!! url('sosmedPeople/create?person_id='.$person->id) !!}">Sosmed +</a>--}}
{{--                <a class="btn btn-sm btn-success" href="{!! url('personHasPendidikans/create?person_id='.$person->id) !!}">Pendidikan +</a>--}}
{{--            </td>--}}
            <td>
                {!! Form::open(['route' => ['people.destroy', $person->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('people.show', [$person->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                    <a href="{!! route('people.edit', [$person->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
