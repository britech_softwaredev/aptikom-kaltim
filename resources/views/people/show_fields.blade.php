<div class="row">
    <div class="col-lg-12 mb-2">
        <div class="text-center justify-content-center">
            <img src="{{ $person->getFirstMediaUrl('default','thumb') }}" alt="avatar" class="height-20 img-fluid">
            <div class="font-large-1 black text-bold-700 mt-2">{!! $person->name ?? '' !!}</div>
            <p class="black">No. Anggota : {!! $person->no_anggota ?? '' !!}</p>
        </div>
    </div>
    <div class="col-lg-4 mb-2">
       <div class="card border-blue-grey border-lighten-4">
           <div class="card-body">
               <p class="font-medium-1 text-bold-800 black mb-2 text-uppercase">Biodata Anggota</p>
               <div class="mb-0-1">
                   <span class="font-medium-1">No Anggota</span>
                   <p class="font-medium-1 text-bold-600 black mb-0">{!! $person->no_anggota ?? '' !!}</p>
               </div>
               <div class="mb-0-1">
                   <span>Nama Lengkap</span>
                   <p class="font-medium-1 text-bold-600 black mb-0">{!! $person->name !!}</p>
               </div>
{{--               <div class="mb-0-1">--}}
{{--                   <span>Jabatan</span>--}}
{{--                   <p class="font-medium-1 text-bold-600 black mb-0">{!! $person['jabatan']['name'] ?? '' !!}</p>--}}
{{--               </div>--}}
               <div class="mb-0-1">
                   <span>Tempat & Tanggal Lahir</span>
                   <p class="font-medium-1 text-bold-600 black mb-0">{!! $person->place_of_birth ?? '' !!}, {!! $person->date_of_birth ? \Carbon\Carbon::parse($person->date_of_birth)->format('d F Y') : '' !!}</p>
               </div>
               <div class="mb-0-1">
                   <span class="font-medium-1">Alamat</span>
                   <p class="font-medium-1 text-bold-600 black mb-0">{!! $person->address ?? '' !!}</p>
               </div>
           </div>
       </div>
    </div>
    <div class="col-lg-4 mb-20">
        <div class="card border-blue-grey border-lighten-4">
            <div class="card-body">
                <p class="font-medium-1 text-bold-800 black mb-2 text-uppercase">Riwayat Pendidikan</p>
                @foreach($person['personHasPendidikans'] as $item)
                    <div class="mb-1">
                        <p class="black mb-0 text-bold-700">{!! $item['pendidikan']['name'] ?? ''!!}</p>
                        <p class="black mb-0">- {!! $item->perguruan_tinggi !!}</p>
                        <p class="black mb-0">- {!! $item->program_studi !!}</p>
                        <p class="black mb-0">- {!! $item->tahun_lulus !!}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-lg-4 mb-20">
        <div class="card border-blue-grey border-lighten-4">
            <div class="card-body">
                <p class="font-medium-1 text-bold-800 black mb-2 text-uppercase">Portofolio</p>
                @foreach($person['personPorto'] as $item)
                    <div class="mb-1">
                        <p class="black mb-0 text-bold-700">{!! $item->name !!}</p>
                        <p class="black mb-0">- {!! $item->year !!}</p>
                        <p class="black mb-0">- {!! $item->company !!}</p>
                        <p class="black mb-0"><a href="{!! $item->link !!}">- {!! $item->link !!}</a></p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-lg-4 mb-20">
        <div class="card border-blue-grey border-lighten-4">
            <div class="card-body">
                <p class="font-medium-1 text-bold-800 black mb-2 text-uppercase">Kemampuan</p>
                @foreach($person['personSkill'] as $item)
                    <div class="mb-1">
                        <p class="black mb-0 text-bold-600">- {!! $item['skill']->name !!}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-lg-4 mb-20">
        <div class="card border-blue-grey border-lighten-4">
            <div class="card-body">
                <p class="font-medium-1 text-bold-800 black mb-2 text-uppercase">Sosial Media</p>
                @foreach($person['sosmed'] as $item)
                    <div class="mb-1">
                        <p class="black mb-0 text-bold-700">{!! $item->key !!}</p>
                        <p class="black mb-0">- {!! $item->value !!}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

