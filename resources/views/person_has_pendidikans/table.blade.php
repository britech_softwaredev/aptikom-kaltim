<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default table-responsive">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Person Id</th>
        <th>Pendidikan Id</th>
        <th>Perguruan Tinggi</th>
        <th>Program Studi</th>
        <th>Tahun Lulus</th>
        <th>Person Has Pendidikancol</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($personHasPendidikans as $personHasPendidikan)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $personHasPendidikan->person_id !!}</td>
            <td>{!! $personHasPendidikan->pendidikan_id !!}</td>
            <td>{!! $personHasPendidikan->perguruan_tinggi !!}</td>
            <td>{!! $personHasPendidikan->program_studi !!}</td>
            <td>{!! $personHasPendidikan->tahun_lulus !!}</td>
            <td>{!! $personHasPendidikan->person_has_pendidikancol !!}</td>
            <td>
                {!! Form::open(['route' => ['personHasPendidikans.destroy', $personHasPendidikan->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('personHasPendidikans.show', [$personHasPendidikan->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('personHasPendidikans.edit', [$personHasPendidikan->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
