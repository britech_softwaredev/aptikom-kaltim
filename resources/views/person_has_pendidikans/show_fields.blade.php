<!-- Sudah di modifikasi -->
<!-- Person Id Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('person_id', 'Person Id:') !!}
        </h5>
        {!! $personHasPendidikan->person_id !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Pendidikan Id Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('pendidikan_id', 'Pendidikan Id:') !!}
        </h5>
        {!! $personHasPendidikan->pendidikan_id !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Perguruan Tinggi Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('perguruan_tinggi', 'Perguruan Tinggi:') !!}
        </h5>
        {!! $personHasPendidikan->perguruan_tinggi !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Program Studi Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('program_studi', 'Program Studi:') !!}
        </h5>
        {!! $personHasPendidikan->program_studi !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Tahun Lulus Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('tahun_lulus', 'Tahun Lulus:') !!}
        </h5>
        {!! $personHasPendidikan->tahun_lulus !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Person Has Pendidikancol Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('person_has_pendidikancol', 'Person Has Pendidikancol:') !!}
        </h5>
        {!! $personHasPendidikan->person_has_pendidikancol !!}
    </div>
</div>

