

<input type="text" id="person_id" name="person_id" value="{{ $person_id ?? $personHasPendidikan->person_id }}" hidden>

<!-- Pendidikan Id Field -->
<div class="form-group">
    {!! Form::label('pendidikan_id', 'Pendidikan') !!}
    <div class="position-relative">
        {!! Form::select('pendidikan_id',$pendidikan,null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Perguruan Tinggi Field -->
<div class="form-group">
    {!! Form::label('perguruan_tinggi', 'Perguruan Tinggi:') !!}
    <div class="position-relative">
        {!! Form::text('perguruan_tinggi', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Program Studi Field -->
<div class="form-group">
    {!! Form::label('program_studi', 'Program Studi:') !!}
    <div class="position-relative">
        {!! Form::text('program_studi', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Tahun Lulus Field -->
<div class="form-group">
    {!! Form::label('tahun_lulus', 'Tahun Lulus:') !!}
    <div class="position-relative">
        {!! Form::text('tahun_lulus', null, ['class' => 'form-control']) !!}
    </div>
</div>


{{--<!-- Person Has Pendidikancol Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('person_has_pendidikancol', 'Person Has Pendidikancol:') !!}--}}
{{--    <div class="position-relative">--}}
{{--        {!! Form::text('person_has_pendidikancol', null, ['class' => 'form-control']) !!}--}}
{{--    </div>--}}
{{--</div>--}}

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! url('people') !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

