<!-- Title Field -->
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('title', 'Judul:') !!}
        <div class="position-relative">
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Post Category Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('post_category_id', 'Kategori Post :') !!}
        <div class="position-relative">
            {!! Form::select('post_category_id',$postCategories, null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group col-sm-12">
        {!! Form::label('image_caption', 'Judul Foto :') !!}
        <div class="position-relative">
            {!! Form::text('image_caption', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('foto', 'Foto :') !!}
    <div class="position-relative">
        @if(!isset($post))
            <x-media-library-attachment  multiple name="media" rules="mimes:png,jpeg,pdf"/>
        @else
            <x-media-library-collection :model="$post"  name="media" rules="mimes:png,jpeg,pdf"/>
        @endif
    </div>
</div>


<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <div class="position-relative">
        {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('created_at', 'Tanggal :') !!}
    <div class="position-relative">
        {!! Form::Date('created_at', isset($post)?$post->created_at->format('Y-m-d'):\Carbon\Carbon::today(), ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! route('posts.index') !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/media-pro/styles.css')}}">

    <livewire:styles />
    <livewire:scripts />

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.min.js" defer></script>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('ckeditor/adapters/jquery.js') }}"></script>
    <script  type="text/javascript">
        setTimeout(function(){
            CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
            CKEDITOR.replace( 'content');
        },300);
    </script>
@endsection

