@extends('layouts.app')
@section('content')
    <div class="content-body">
        @role('administrator')
            <div class="mb-5 mt-1">
                <p class="font-large-2 black text-bold-700 mb-0">Selamat Datang</p>
                <p class="font-medium-3 text-bold-500">Halaman untuk anda pengelola administrator Website Aptikom Kalimantan Timur.</p>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-6 col-12">
                    <div class="card bg-info">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-center">
                                        <i class="fa fa-users text-white font-large-2 float-left"></i>
                                    </div>
                                    <div class="media-body text-white text-right">
                                        <h1 class="text-white">{{ \App\Models\Person::count() }}</h1>
                                        <span>Anggota Aptikom</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-12">
                    <div class="card bg-danger">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-center">
                                        <i class="icon-news text-white font-large-2 float-left"></i>
                                    </div>
                                    <div class="media-body text-white text-right">
                                        <h1 class="text-white">{{ \App\Models\Post::count() }}</h1>
                                        <span>Berita Terkabarkan</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-12">
                    <div class="card bg-success">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-center">
                                        <i class="fa fa-user text-white font-large-2 float-left"></i>
                                    </div>
                                    <div class="media-body text-white text-right">
                                        <h1 class="text-white">{{ \App\Models\User::count() }}</h1>
                                        <span>Akun Pengguna</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endrole

        @role('anggota')
            <div class="mb-5 mt-1">
                <p class="font-large-2 black text-bold-700 mb-0">Selamat Datang Anggota</p>
                <p class="font-medium-3 text-bold-500">Ubah biodata serta informasi diri anda sebagai anggota aptikom pada menu akun.</p>
            </div>
        @endrole
    </div>
@endsection
