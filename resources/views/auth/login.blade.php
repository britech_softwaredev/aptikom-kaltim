<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login - Website-Aptikom</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico" />

    <!-- font -->
    <link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900|Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">

    <!-- Plugins -->
    <link rel="stylesheet" type="text/css" href="{{ asset('master/web-assets/css/plugins-css.css') }}" />

    <!-- Typography -->
    <link rel="stylesheet" type="text/css" href="{{ asset('master/web-assets/css/typography.css') }}" />

    <!-- Shortcodes -->
    <link rel="stylesheet" type="text/css" href="{{ asset('master/web-assets/css/shortcodes/shortcodes.css') }}" />

    <!-- Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('master/web-assets/css/style.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('master/web-assets/demo-categories/css/menu-center.css') }}" />

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="{{ asset('master/web-assets/css/responsive.css') }}" />


</head>

<body>

<div class="wrapper">

    <!--=================================
     preloader -->

{{--    <div id="pre-loader">--}}
{{--        <img src="images/pre-loader/loader-01.svg" alt="">--}}
{{--    </div>--}}

    <!--=================================
     preloader -->

    <!--=================================
    login -->

    <section class="height-100vh d-flex align-items-center page-section-ptb login" style="background-color: #0099FF;">
        <div class="container">
            <div class="row no-gutters justify-content-center">
                <div class="col-lg-4 col-md-6 login-fancy-bg bg-overlay-black-20">

                    <div class="login-fancy pos-r">
                        <h2 class="text-white mb-20">Hello admin!</h2>
                        <p class="mb-20 text-white">Selamat Datang Admin Website APTIKOM Provinsi Kalimantan Timur.</p>
                        <ul class="list-unstyled pos-bot pb-30">
                            <li class="list-inline-item"><a class="text-white">@copyright {{ \Carbon\Carbon::now()->format('Y') }}</a> </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 white-bg">
                    <form class="login-fancy pb-40 clearfix" method="post" action="{{ url('/login') }}">
                        @csrf

                        <h3 class="mb-30 text-bold-700">Login</h3>
                        <div class="section-field mb-20">
                            <label class="mb-10" for="name">Email Admin </label>
{{--                            <input id="name" class="web form-control" type="email" placeholder="dprd.kaltim@support.com" name="web">--}}
                            <input type="email"
                                   name="email"
                                   value="{{ old('email') }}"
                                   placeholder="Masukan Email"
                                   class="web form-control @error('email') is-invalid @enderror">
                            @error('email')
                                <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="section-field mb-20">
                            <label class="mb-10" for="Password">Password </label>
{{--                            <input id="Password" class="Password form-control" type="password" placeholder="Password" name="Password">--}}
                            <input type="password"
                                   name="password"
                                   placeholder="Masukan Password"
                                   class="Password form-control @error('password') is-invalid @enderror">
                        </div>

                        <div class="section-field mb-20">
                            <label class="mb-10" for="Captcha">Captcha </label>
                            <div class="refereshrecaptcha">
                                {!! captcha_img('flat') !!}
                            </div>
                            <a href="javascript:void(0)" onclick="refreshCaptcha()">Refresh Captcha</a>
                            <input type="text"
                                   maxlength="6"
                                   placeholder="Masukkan Kode Captcha"
                                   name="captcha"
                                   class="Captcha form-control @error('captcha') is-invalid @enderror">


                            <script>
                                function refreshCaptcha(){
                                    $.ajax({
                                        url: "/refereshcaptcha",
                                        type: 'get',
                                        dataType: 'html',
                                        success: function(json) {
                                            $('.refereshrecaptcha').html(json);
                                        },
                                        error: function(data) {
                                            alert('Try Again.');
                                        }
                                    });
                                }
                            </script>

                        </div>
{{--                        <div class="section-field">--}}
{{--                            <div class="custom-control custom-checkbox mb-30">--}}
{{--                                <input type="checkbox" class="custom-control-input" id="customControlAutosizing">--}}
{{--                                <label class="custom-control-label" for="customControlAutosizing">Remember me</label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <button type="submit" class="btn btn-warning ">
                            <span>Log in</span>
                            <i class="fa fa-check"></i>
                        </button>
{{--                        <p class="mt-20 mb-0">Don't have an account? <a href="signup-05.html"> Create one here</a></p>--}}
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
     login -->

</div>

<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a></div>

<!-- jquery -->
<script src="{{ asset('master/web-assets/js/jquery-3.4.1.min.js') }}"></script>

<script src="{{ asset('js/share.js') }}"></script>

<!-- plugins-jquery -->
<script src="{{ asset('master/web-assets/js/plugins-jquery.js') }}"></script>

<!-- plugin_path -->
<script>var plugin_path = '{{  asset('master/web-assets/js/') }}/';</script>

<!-- custom -->
<script src="{{ asset('master/web-assets/js/custom.js') }}"></script>


</body>
</html>
