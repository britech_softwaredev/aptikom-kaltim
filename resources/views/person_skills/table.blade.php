<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<div class=" table-responsive">
<table class="table table-hover table-bordered table-striped default">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Person Name</th>
        <th>Skill</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($personSkills as $personSkill)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $personSkill['person']['name'] !!}</td>
            <td><span class="badge badge-info text-bold-700 font-medium-1">{!! $personSkill['skill']['name'] !!}</span></td>
            <td>
                {!! Form::open(['route' => ['personSkills.destroy', $personSkill->id], 'method' => 'delete']) !!}
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="{!! route('personSkills.show', [$personSkill->id]) !!}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
                                       <a href="{!! route('personSkills.edit', [$personSkill->id]) !!}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                       {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
