<!-- Skill Id Field -->
<div class="form-group">
    {!! Form::label('skill_id', 'Skill Id:') !!}
    <div class="position-relative">
        {!! Form::select('skill_id', $skill,null, ['class' => 'form-control']) !!}
    </div>
</div>

<input type="text" id="person_id" name="person_id" value="{{ $person_id ?? $personSkill->person_id }}" hidden>

{{--<!-- Person Id Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('person_id', 'Person Id:') !!}--}}
{{--    <div class="position-relative">--}}
{{--        {!! Form::select('person_id', $person,null, ['class' => 'form-control']) !!}--}}
{{--    </div>--}}
{{--</div>--}}

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! url()->previous() !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

