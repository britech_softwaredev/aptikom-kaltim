<ul class="nav nav-tabs mb-30" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link {{$activeAll}} show mb-10 font-small-3" wire:click="allCategories()" id="tab-all-content" data-toggle="tab" href="#tab-all" role="tab" aria-controls="tab-all" aria-selected="true">Semua</a>
    </li>
    {{--@foreach($postCategories as $postCategory)
        <li class="nav-item">
            <a class="nav-link @if($postCategory->id==$idCategory) active show @endif mb-10 font-small-3" wire:click="showPosts({{$postCategory->id}})"  data-toggle="tab" href="#tab-{{$postCategory->name}}" role="tab" aria-controls="tab-{{$postCategory->name}}" aria-selected="true">
                @if (Config::get('app.locale') == 'en')
                    {{$postCategory->name_english}}
                @elseif ( Config::get('app.locale') == 'id' )
                    {{$postCategory->name}}
                @endif
            </a>
        </li>
    @endforeach--}}
</ul>

<div class="case-item-wrap">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
        <div class="case-item">
            <div class="case-item__thumb mb-0">
                <article class="hentry post post-standard clear">
                    <div class="post-thumb clear">
                        <img src="{{ asset('master/website-assets/img/PORTO/porto1.png') }}" alt="seo">
                        <div class="overlay"></div>
                        <a href="{{ asset('master/website-assets/img/PORTO/porto1.png') }}" class="link-image js-zoom-image">
                            <i class="seoicon-zoom"></i>
                        </a>
                        <a href="#" class="link-post">
                            <i class="seoicon-link-bold"></i>
                        </a>
                    </div>
                </article>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
        <div class="case-item">
            <div class="case-item__thumb mb-0">
                <article class="hentry post post-standard clear">
                    <div class="post-thumb clear">
                        <img src="{{ asset('master/website-assets/img/PORTO/porto2.png') }}" alt="seo">
                        <div class="overlay"></div>
                        <a href="{{ asset('master/website-assets/img/PORTO/porto2.png') }}" class="link-image js-zoom-image">
                            <i class="seoicon-zoom"></i>
                        </a>
                        <a href="#" class="link-post">
                            <i class="seoicon-link-bold"></i>
                        </a>
                    </div>
                </article>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
        <div class="case-item">
            <div class="case-item__thumb mb-0" data-offset="5">
                <article class="hentry post post-standard clear">
                    <div class="post-thumb clear">
                        <img src="{{ asset('master/website-assets/img/PORTO/porto3.png') }}" alt="seo">
                        <div class="overlay"></div>
                        <a href="{{ asset('master/website-assets/img/PORTO/porto3.png') }}" class="link-image js-zoom-image">
                            <i class="seoicon-zoom"></i>
                        </a>
                        <a href="#" class="link-post">
                            <i class="seoicon-link-bold"></i>
                        </a>
                    </div>
                </article>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
        <div class="case-item">
            <div class="case-item__thumb mb-0" data-offset="5">
                <article class="hentry post post-standard clear">
                    <div class="post-thumb clear">
                        <img src="{{ asset('master/website-assets/img/PORTO/porto4.png') }}" alt="seo">
                        <div class="overlay"></div>
                        <a href="{{ asset('master/website-assets/img/PORTO/porto4.png') }}" class="link-image js-zoom-image">
                            <i class="seoicon-zoom"></i>
                        </a>
                        <a href="#" class="link-post">
                            <i class="seoicon-link-bold"></i>
                        </a>
                    </div>
                </article>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
        <div class="case-item">
            <div class="case-item__thumb mb-0">
                <article class="hentry post post-standard clear">
                    <div class="post-thumb clear">
                        <img src="{{ asset('master/website-assets/img/PORTO/porto8.png') }}" alt="seo">
                        <div class="overlay"></div>
                        <a href="{{ asset('master/website-assets/img/PORTO/porto8.png') }}" class="link-image js-zoom-image">
                            <i class="seoicon-zoom"></i>
                        </a>
                        <a href="#" class="link-post">
                            <i class="seoicon-link-bold"></i>
                        </a>
                    </div>
                </article>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
        <div class="case-item">
            <div class="case-item__thumb mb-0">
                <article class="hentry post post-standard clear">
                    <div class="post-thumb clear">
                        <img src="{{ asset('master/website-assets/img/PORTO/porto5.png') }}" alt="seo">
                        <div class="overlay"></div>
                        <a href="{{ asset('master/website-assets/img/PORTO/porto5.png') }}" class="link-image js-zoom-image">
                            <i class="seoicon-zoom"></i>
                        </a>
                        <a href="#" class="link-post">
                            <i class="seoicon-link-bold"></i>
                        </a>
                    </div>
                </article>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
        <div class="case-item">
            <div class="case-item__thumb mb-0">
                <article class="hentry post post-standard clear">
                    <div class="post-thumb clear">
                        <img src="{{ asset('master/website-assets/img/PORTO/porto6.png') }}" alt="seo">
                        <div class="overlay"></div>
                        <a href="{{ asset('master/website-assets/img/PORTO/porto6.png') }}" class="link-image js-zoom-image">
                            <i class="seoicon-zoom"></i>
                        </a>
                        <a href="#" class="link-post">
                            <i class="seoicon-link-bold"></i>
                        </a>
                    </div>
                </article>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
        <div class="case-item">
            <div class="case-item__thumb mb-0">
                <article class="hentry post post-standard clear">
                    <div class="post-thumb clear">
                        <img src="{{ asset('master/website-assets/img/PORTO/porto7.png') }}" alt="seo">
                        <div class="overlay"></div>
                        <a href="{{ asset('master/website-assets/img/PORTO/porto7.png') }}" class="link-image js-zoom-image">
                            <i class="seoicon-zoom"></i>
                        </a>
                        <a href="#" class="link-post">
                            <i class="seoicon-link-bold"></i>
                        </a>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>
