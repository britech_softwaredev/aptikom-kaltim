<div class="modal fade text-left" id="editProfil" tabindex="-1" role="dialog" aria-labelledby="editProfil"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title font-medium-1 text-text-bold-700 black" id="myModalLabel33">Perubahan Data Akun</label>
                <button type="button" class="close danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x"></i></span>
                </button>
            </div>
            <div class="card-body">
                {!! Form::model($user, ['url' => 'updateProfile/'.$user->id, 'method' => 'patch','class'=>'form form-horizontal','files' => true]) !!}
                    <div class="form-body">

                        <div class="row">
                            <div class="col-6">
                                <!-- Name Field -->
                                <div class="form-group">
                                    {!! Form::label('name', 'Username',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user->name }}</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <!-- Username Field -->
                                <div class="form-group">
                                    {!! Form::label('username', 'Name Akun',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user->display_name }}</div>
                                </div>

                            </div>
                            <div class="col-12">
                                <!-- Email Field -->
                                <div class="form-group">
                                    {!! Form::label('email', 'Email Aktif',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user->email }}</div>
                                </div>
                            </div>
                            @if(!empty(Auth::user()['person']))
                            <div class="col-12 mb-2 mt-2">
                                <p class="font-medium-3 text-bold-700 black">Biodata Anggota</p>
                                {!! Form::label('foto', 'Foto',['class'=>' text-uppercase']) !!}
                                @if(!isset($user['person']))
                                    <x-media-library-attachment name="media" rules="mimes:png,jpeg"/>
                                @else
                                    <x-media-library-collection :model="$user['person']" max-items="1"  name="media" rules="mimes:png,jpeg"/>
                                @endif
                            </div>
                            <div class="col-6">
                                <!-- No Hp Field -->
                                <div class="form-group">
                                    {!! Form::label('name_full', 'Nama Lengkap',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user['person']->name_full }}</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <!-- No Hp Field -->
                                <div class="form-group">
                                    {!! Form::label('place_of_birth', 'Tempat Lahir',['class'=>' text-uppercase']) !!}
                                    {!! Form::text('place_of_birth',$user['person']->place_of_birth, ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <!-- No Hp Field -->
                                <div class="form-group">
                                    {!! Form::label('date_of_birth', 'Tanggal Lahir',['class'=>' text-uppercase']) !!}
                                    {!! Form::date('date_of_birth', $user['person']->date_of_birth ? date('Y-m-d',strtotime($user['person']->date_of_birth)) : '' , ['class' => 'custom-select border-left-6 text-bold-600 black']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <!-- No Hp Field -->
                                <div class="form-group">
                                    {!! Form::label('no_hp', 'No Hp/Telephone',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user['person']->no_hp }}</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <!-- No Hp Field -->
                                <div class="form-group">
                                    {!! Form::label('email_valid', 'Email',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user['person']->email_valid }}</div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('address', 'Alamat Lengkap',['class'=>' text-uppercase']) !!}
                                    {!! Form::textarea('address', $user['person']->address, ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black','rows'=>'2','maxlength'=>'255']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <!-- No Hp Field -->
                                <div class="form-group">
                                    {!! Form::label('perguruan_tinggi', 'Perguruan Tinggi',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user['person']->perguruan_tinggi }}</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <!-- No Hp Field -->
                                <div class="form-group">
                                    {!! Form::label('program_studi', 'Program Studi',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user['person']->program_studi }}</div>
                            </div>
</div>
                            <div class="col-6">
                                <!-- No Hp Field -->
                                <div class="form-group">
                                    {!! Form::label('jabatan_perguruan_tinggi', 'Jabatan Perguruan Tinggi',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user['person']->jabatan_perguruan_tinggi }}</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <!-- No Hp Field -->
                                <div class="form-group">
                                    {!! Form::label('alamat_perguruan_tinggi', 'Alamat Perguruan Tinggi',['class'=>' text-uppercase']) !!}
                                    <div class="form-control border-left-pink border-left-6 text-bold-600 black">{{ $user['person']->alamat_perguruan_tinggi }}</div>
                                </div>
                            </div>
                            @endif

                            <div class="col-12">
                                <div class="card">
                                    <div id="heading1" class="card-header p-1 rounded-2 border-left-6 border-left-green box-shadow-0-1 cursor-pointer" role="tab" data-toggle="collapse" data-parent="#accordionWrapa1" href="#steap1">
                                        <a data-toggle="collapse" data-parent="#accordionWrapa1" href="#steap1" aria-expanded="false" aria-controls="accordion1" class="font-small-3 text-bold-800 black text-uppercase">
                                            <i class="fa fa-gears green mr-1"></i> Pengaturan Kata Sandi
                                        </a>
                                    </div>
                                    <div id="steap1" role="tabpanel" aria-labelledby="heading1" class="collapse">
                                        <div class="card-content">
                                            <div class="card-body rounded-0-5">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        {!! Form::label('current_password', 'Password Lama',['class'=>' text-uppercase']) !!}
                                                        <div class="position-relative has-icon-right">
                                                            {!! Form::password('current_password', ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black font-medium-2','autocomplete'=>'off']) !!}
                                                            <div class="form-control-position">
                                                                <i class="fa fa-eye font-medium-3 toggle-password" toggle="#current_password"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        {!! Form::label('password', 'Password Baru',['class'=>' text-uppercase']) !!}
                                                        <div class="position-relative has-icon-right">
                                                            {!! Form::password('password', ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black font-medium-2','autocomplete'=>'off']) !!}
                                                            <div class="form-control-position">
                                                                <i class="fa fa-eye font-medium-3 toggle-password" toggle="#password"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        {!! Form::label('password_confirmation', 'Konfirmasi Password Baru',['class'=>' text-uppercase']) !!}
                                                        <div class="position-relative has-icon-right">
                                                            {!! Form::password('password_confirmation', ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black font-medium-2','autocomplete'=>'off']) !!}
                                                            <div class="form-control-position">
                                                                <i class="fa fa-eye font-medium-3 toggle-password" toggle="#password_confirmation"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="text-right">
                    {!! Form::submit('Simpan Perubahan', ['class' => 'btn btn-green rounded-2 btn-glow mr-1']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
