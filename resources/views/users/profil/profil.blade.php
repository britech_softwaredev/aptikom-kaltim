@extends('layouts.app')
@section('content')
<div id="user">
    @include('flash::message')
{{--    @canany(['profilAdm.index','superAdmin.index','profilIndex.index'])--}}
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card box-shadow-1 rounded-1">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 text-center">
                                    @if(empty(Auth::user()['person']))
                                        <img src="{{ asset('master/app-assets/images/gallery/user_profil.png') }}" class="img-fluid rounded height-150">
                                    @else
                                        <img src="{{ Auth::user()['person']->getFirstMediaUrl('default','thumb') }}" class="rounded-circle img-fluid ">
                                    @endif
                                    <div class="font-medium-2 text-bold-700 black mt-2">{{ Auth::user()['display_name'] }}</div>
                                    <div class="font-medium-1 black">{{ Auth::user()->email }}</div>
                                    <a data-target="#editProfil" data-toggle="modal" href="javascript:void(0)"  class="btn btn-primary btn-sm mt-2">Ubah Profil</a>
                                    @include('users.modal.edit_profile')
                                </div>
                                <div class="col-lg-6">
                                    <table class="table black mb-0 font-small-4">
                                        <tr>
                                            <td colspan="3" class="p-1 border-top-0">
                                                <div class="font-medium-4 text-bold-800 black">Data Akun</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="p-1">Username</td>
                                            <td class="p-1">:</td>
                                            <td class="p-1">{{ Auth::user()['name'] }}</td>
                                        </tr>
                                        <tr>
                                            <td class="p-1">Nama</td>
                                            <td class="p-1">:</td>
                                            <td class="p-1">{{ Auth::user()['display_name'] }}</td>
                                        </tr>
                                        <tr>
                                            <td class="p-1">Email</td>
                                            <td class="p-1">:</td>
                                            <td class="p-1">{{ Auth::user()['email'] }}</td>
                                        </tr>
                                        <tr>
                                            <td class="p-1">Bergabung</td>
                                            <td class="p-1">:</td>
                                            <td class="p-1">{{ Auth::user()->created_at->format('d F Y') }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!empty(Auth::user()['person']))
                    <div class="col-7">
                        <div class="card box-shadow-1 rounded-1">
                            <div class="card-body pt-0">
                                <table class="table black mb-0 font-small-4">
                                    <tr>
                                        <td colspan="3" class="p-1 border-top-0">
                                            <div class="font-medium-4 text-bold-800 black">Data Anggota</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">No. Anggota</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person'] ? Auth::user()['person']['no_anggota'] : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Masa Berlaku</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">
                                            @if(!empty(Auth::user()['person']) && !empty(Auth::user()['person']['masa_berlaku']))
                                                @if(date('Y',strtotime(Auth::user()['person']['masa_berlaku'])) < \Carbon\Carbon::now()->format('Y'))
                                                   {{ Auth::user()['person'] ? Auth::user()['person']['masa_berlaku'] : '' }}  <span class="red text-bold-700">  KADALUARSA</span>
                                                @else
                                                    {{ Auth::user()['person'] ? Auth::user()['person']['masa_berlaku'] : '' }}
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Nama Lengkap</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person'] ? Auth::user()['person']['name_full'] : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Tempat, Tanggal Lahir</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person']['place_of_birth'] ? Auth::user()['person']['place_of_birth'] : '' }}, {{ Auth::user()['person']['date_of_birth'] ? Auth::user()['person']['date_of_birth']->format('d F Y') : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Alamat</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person'] ? Auth::user()['person']['address'] : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Email</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person'] ? Auth::user()['person']['email_valid'] : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Kontak</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person'] ? Auth::user()['person']['no_hp'] : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Perguruan Tinggi</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person'] ? Auth::user()['person']['perguruan_tinggi'] : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Program Studi</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person'] ? Auth::user()['person']['program_studi'] : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Jabatan</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person'] ? Auth::user()['person']['jabatan_perguruan_tinggi'] : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-1">Alamat Perguruan Tinggi</td>
                                        <td class="p-1">:</td>
                                        <td class="p-1">{{ Auth::user()['person'] ? Auth::user()['person']['alamat_perguruan_tinggi'] : '' }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="card box-shadow-1 rounded-1">
                            <div class="card-body pt-0">
                                <div class="font-medium-4 text-bold-800 black mt-1 mb-2">Sosial Media <a target="_blank" href="{!! url('sosmedPeople/create?person_id='.Auth::user()['person']->id) !!}"><i class="fa fa-plus green"></i></a></div>
                                @foreach(Auth::user()['person']['sosmed'] as $item)
                                    <div class="mb-1">
                                        {!! Form::open(['route' => ['sosmedPeople.destroy', $item->id], 'method' => 'delete']) !!}
                                            <div class="d-flex">
                                                {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit','class'=>'btn btn-sm btn-outline-danger rounded-2 justify-content-center justify-content-center align-content-center mr-0-1', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                <div>
                                                    <p class="black mb-0 text-bold-700">{!! $item->key !!}
                                                        <a href="{{ url('sosmedPeople/'.$item->id.'/edit') }}"><i class="fa fa-pencil-square"></i></a>
                                                    </p>
                                                    <p class="black mb-0">- {!! $item->value !!}</p>
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card box-shadow-1 rounded-1">
                            <div class="card-body pt-0">
                                <div class="font-medium-4 text-bold-800 black mt-1 mb-2">Riwayat Pendidikan <a target="_blank" href="{!! url('personHasPendidikans/create?person_id='.Auth::user()['person']->id) !!}"><i class="fa fa-plus green"></i></a></div>
                                @foreach(Auth::user()['person']['personHasPendidikans'] as $item)
                                    <div class="mb-1">
                                        {!! Form::open(['route' => ['personHasPendidikans.destroy', $item->id], 'method' => 'delete']) !!}
                                        <div class="d-flex">
                                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit','class'=>'btn btn-sm btn-outline-danger rounded-2 justify-content-center justify-content-center align-content-center mr-0-1', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                            <div>
                                                <p class="black mb-0 text-bold-700">{!! $item['pendidikan']['name'] ?? ''!!}
                                                    <a href="{{ url('personHasPendidikans/'.$item->id.'/edit') }}"><i class="fa fa-pencil-square"></i></a>
                                                </p>
                                                <p class="black mb-0">- {!! $item->perguruan_tinggi !!}</p>
                                                <p class="black mb-0">- {!! $item->program_studi !!}</p>
                                                <p class="black mb-0">- Lulus {!! $item->tahun_lulus !!}</p>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card box-shadow-1 rounded-1">
                            <div class="card-body pt-0">
                                <div class="font-medium-4 text-bold-800 black mt-1 mb-2">Portofolio <a target="_blank" href="{!! url('personPortofolios/create?person_id='.Auth::user()['person']->id) !!}"><i class="fa fa-plus green"></i></a></div>
                                @foreach(Auth::user()['person']['personPorto'] as $item)
                                    <div class="mb-1">
                                        {!! Form::open(['route' => ['personPortofolios.destroy', $item->id], 'method' => 'delete']) !!}
                                        <div class="d-flex">
                                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit','class'=>'btn btn-sm btn-outline-danger rounded-2 justify-content-center justify-content-center align-content-center mr-0-1', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                            <div>
                                                <p class="black mb-0 text-bold-700">{!! $item->name !!}
                                                    <a href="{{ url('personPortofolios/'.$item->id.'/edit') }}"><i class="fa fa-pencil-square"></i></a>
                                                </p>
                                                <p class="black mb-0">- {!! $item->year !!}</p>
                                                <p class="black mb-0">- {!! $item->company !!}</p>
                                                <p class="black mb-0"><a href="{!! $item->link !!}">- {!! $item->link !!}</a></p>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card box-shadow-1 rounded-1">
                            <div class="card-body pt-0">
                                <div class="font-medium-4 text-bold-800 black mt-1 mb-2">Kemampuan <a target="_blank" href="{!! url('personSkills/create?person_id='.Auth::user()['person']->id) !!}"><i class="fa fa-plus green"></i></a></div>
                                @foreach(Auth::user()['person']['personSkill'] as $item)
                                    <div class="mb-1">
                                        {!! Form::open(['route' => ['personSkills.destroy', $item->id], 'method' => 'delete']) !!}
                                        <div class="d-flex">
                                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit','class'=>'btn btn-sm btn-outline-danger rounded-2 justify-content-center justify-content-center align-content-center mr-0-1', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                            <div>
                                                <p class="black mb-0 text-bold-600">- {!! $item['skill']->name !!}
                                                    <a href="{{ url('personSkills/'.$item->id.'/edit') }}"><i class="fa fa-pencil-square"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/media-pro/styles.css')}}">

    <livewire:styles />
    <livewire:scripts />

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.min.js" defer></script>
@endsection
@section('scripts')
{{--    <script src="{{ asset('js/user.js') }}"></script>--}}
    <script src="{{asset('master/app-assets/js/scripts/forms/form-login-register.js')}}" type="text/javascript"></script>
@endsection

