
<div class="card rounded-2 box-shadow-1 mb-5">
    <div class="card-content">
        <div class="card-body">
            <div class="font-medium-2 text-bold-800 black mb-2"><i class="fa fa-pencil-square pr-1 green"></i> DATA AKUN</div>
            {{--<select name="unit_id" id="unit_id">--}}
            {{--    <option value="">null</option>--}}
            {{--</select>--}}
            <div class="row">
                <div class="col-6">
                    <!-- Name Field -->
                    <div class="form-group">
                        {!! Form::label('name', 'Nama Lengkap',['class'=>' text-uppercase']) !!}
                        {!! Form::text('name', null, ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black font-medium-2']) !!}
                    </div>
                </div>
                <div class="col-6">
                    <!-- Username Field -->
                    <div class="form-group">
                        {!! Form::label('display_name', 'Nama Tampil (Nama yang akan muncul di bawah artikel/halaman)',['class'=>' text-uppercase']) !!}
                        {!! Form::text('display_name', null, ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black font-medium-2']) !!}
                    </div>

                </div>
                <div class="col-6">
                    <!-- Email Field -->
                    <div class="form-group">
                        {!! Form::label('email', 'Email Aktif',['class'=>' text-uppercase']) !!}
                        {!! Form::email('email', null, ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black font-medium-2','autocomplete'=>'off']) !!}
                    </div>
                </div>

                <div class="col-6">
                    <!-- Email Field -->
                    <div class="form-group">
                        {!! Form::label('no_anggota', 'No anggota',['class'=>' text-uppercase']) !!}
                        {!! Form::text('no_anggota', $user['person']->no_anggota ?? '', ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black font-medium-2','autocomplete'=>'off']) !!}
                        <p class="text-bold-700 black">Last No Anggota : <span class="danger">{{ \App\Models\Person::orderBy('no_anggota','desc')->latest()->first()->no_anggota ?? '' }}</span></p>
                    </div>
                </div>

                <div class="col-12">
                    <div class="card box-shadow-0-1 mt-1">
                        <div id="heading1" class="card-header rounded-2 border-left-6 border-left-green box-shadow-0-1 cursor-pointer" role="tab" data-toggle="collapse" data-parent="#accordionWrapa1" href="#steap1">
                            <a data-toggle="collapse" data-parent="#accordionWrapa1" href="#steap1" aria-expanded="false" aria-controls="accordion1" class="font-medium-2 text-bold-800 black text-uppercase">
                                <i class="fa fa-lock green mr-1"></i> Kata Sandi Akun
                            </a>
                        </div>
                        <div id="steap1" role="tabpanel" aria-labelledby="heading1" class="collapse show">
                            <div class="card-content">
                                <div class="card-body rounded-0-5 p-3">
                                    <div class="row">
                                        <div class="form-group col-6">
                                            {!! Form::label('password', 'Password',['class'=>' text-uppercase']) !!}
                                            <div class="position-relative has-icon-right">
                                                {!! Form::password('password', ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black font-medium-2','autocomplete'=>'off']) !!}
                                                <div class="form-control-position">
                                                    <i class="fa fa-eye font-medium-3 toggle-password" toggle="#password"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-6">
                                            {!! Form::label('password_confirmation', 'Konfirmasi Password',['class'=>' text-uppercase']) !!}
                                            <div class="position-relative has-icon-right">
                                                {!! Form::password('password_confirmation', ['class' => 'form-control border-left-pink border-left-6 text-bold-600 black font-medium-2','autocomplete'=>'off']) !!}
                                                <div class="form-control-position">
                                                    <i class="fa fa-eye font-medium-3 toggle-password" toggle="#password_confirmation"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card bg-grey bg-lighten-4 rounded-2">
                <div class="d-flex pt-1 pb-1">
                    {!! Form::label('s_role_id', 'Hak Akses Diberikan',['class' => 'col-md-3 label-control text-uppercase mb-0']) !!}
                    <div class="skin skin-flat">
                        @foreach($sRoles as $item)
                            <fieldset>
                                {!! Form::radio('s_role_id[]', $item->id, in_array($item->id, $roles)?true:false,['id'=>'input-'.$item->id]) !!}
                                <label for="input-{{$item->id}}" class="ml-1 text-bold-700 black text-uppercase">{!! $item->name !!} - {!! $item->desc !!}</label>
                            </fieldset>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer fixed-bottom footer-light navbar-border navbar-shadow">
    <div class="form-group row mb-0">
        <div class="col-lg-6 align-self-center">
        </div>
        <div class="col-lg-6 text-right">
            <a href="{!! route('users.index') !!}" class="btn btn-pink rounded-2"> <i class="fa fa-close"></i> Batal</a>
            {!! Form::submit('Simpan', ['class' => 'btn btn-green rounded-2 btn-glow mr-1']) !!}
        </div>
    </div>
</footer>

