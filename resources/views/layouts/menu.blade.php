@hasanyrole('anggota|administrator')
    <li class="{{ Request::is('home') ? 'active' : '' }}">
        <a href="{!! route('home') !!}"><i class="fa fa-home"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Home</span></a>
    </li>
    <li class="{{ Request::is('profil*') ? 'active' : '' }}">
        <a href="{!! route('profil') !!}"><i class="fa fa-user"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Akun</span></a>
    </li>
@endhasanyrole

@role('administrator')
    <li class="navigation-header">
        <span data-i18n="nav.category.layouts">--Data Anggota</span>
        <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Layouts"></i>
    </li>
    <li class="{{ Request::is('people*') ? 'active' : '' }}">
        <a href="{!! route('people.index') !!}"><i class="fa fa-group"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title font-small-4 black">Anggota Aptikom</span></a>
    </li>
    {{--@canany(['posts.index','postCategories.index'])--}}
    <li class="navigation-header">
        <span data-i18n="nav.category.layouts">--Post</span>
        <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Layouts"></i>
    </li>
    <li class=" nav-item"><a href="#"><i class="fa fa-newspaper-o"></i><span class="menu-title font-small-4 black" data-i18n="nav.dash.main">Post</span></a>
        <ul class="menu-content">
            {{--@can('posts.index')--}}
            <li class="{{ Request::is('posts*') ? 'active' : '' }}">
                <a href="{!! route('posts.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Post</span></a>
            </li>
            {{--@endcan--}}
            {{--@can('postCategories.index')--}}
            <li class="{{ Request::is('postCategories*') ? 'active' : '' }}">
                <a href="{!! route('postCategories.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Kategori Post</span></a>
            </li>
            {{--@endcan--}}
        </ul>
    </li>

    <li class="{{ Request::is('galeris*') ? 'active' : '' }}">
        <a href="{!! route('galeris.index') !!}"><i class="fa fa-picture-o"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title font-small-4 black">Galeri</span></a>
    </li>
    <li class="navigation-header">
        <span data-i18n="nav.category.layouts">--Pengaturan</span>
        <i class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Layouts"></i>
    </li>
    <li class="nav-item"><a href="#"><i class="fa fa-users"></i><span class="menu-title font-small-4 black" data-i18n="nav.dash.main">Pengaturan User</span></a>
        <ul class="menu-content">
            <li class="{{ Request::is('users*') ? 'active' : '' }}">
                <a href="{!! route('users.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Akun Pengguna</span></a>
            </li>
            <li class="{{ Request::is('roles*') ? 'active' : '' }}">
                <a href="{!! route('roles.index') !!}"><i class="icon-circle-right"></i><span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Roles</span></a>
            </li>
        </ul>
    </li>
@endrole

@role('guest')

@endrole

{{--@endcanany--}}

