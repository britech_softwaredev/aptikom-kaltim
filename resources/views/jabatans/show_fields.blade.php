<!-- Sudah di modifikasi -->
<!-- Name Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('name', 'Name:') !!}
        </h5>
        {!! $jabatan->name !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Definition Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('definition', 'Definition:') !!}
        </h5>
        {!! $jabatan->definition !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Order Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('order', 'Order:') !!}
        </h5>
        {!! $jabatan->order !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Show In Header Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('show_in_header', 'Show In Header:') !!}
        </h5>
        {!! $jabatan->show_in_header !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Left Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('left', 'Left:') !!}
        </h5>
        {!! $jabatan->left !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Right Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('right', 'Right:') !!}
        </h5>
        {!! $jabatan->right !!}
    </div>
</div>

