{{--<!-- Person Id Field -->--}}
{{--<div class="form-group">--}}
{{--    {!! Form::label('person_id', 'Person') !!}--}}
{{--    <div class="position-relative">--}}
{{--        {!! Form::select('person_id', $person,null, ['class' => 'form-control']) !!}--}}
{{--    </div>--}}
{{--</div>--}}

<input type="text" id="person_id" name="person_id" value="{{ $person_id ?? $sosmedPerson->person_id }}" hidden>

<!-- Key Field -->
<div class="form-group">
    {!! Form::label('key', 'Key') !!} ( facebook, instagram, twitter, youtube, linkedin, google )
    <div class="position-relative">
        {!! Form::text('key', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <div class="position-relative">
        {!! Form::textarea('value', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! url()->previous() !!}" class="btn btn-danger"> <i class="fa fa-close"></i> Batal</a>
    {!! Form::submit('Simpan', ['class' => 'btn btn-green mr-1']) !!}
</div>

