<!-- Sudah di modifikasi -->
<!-- Key Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('key', 'Key:') !!}
        </h5>
        {!! $sosmedPerson->key !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Value Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('value', 'Value:') !!}
        </h5>
        {!! $sosmedPerson->value !!}
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Person Id Field -->
<div class="media">
    <div class="media-body">
        <h5 class="media-heading">
            {!! Form::label('person_id', 'Person Id:') !!}
        </h5>
        {!! $sosmedPerson->person_id !!}
    </div>
</div>

